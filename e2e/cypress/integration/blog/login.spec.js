describe('login form test', function () {
  it('init', function () {
    cy.logout();
  });
  it('login', function () {
    cy.login(Cypress.env('TEST_UID')); // こう書いておけばCIに持っていっても万事OK
    cy.visit('https://hogehoge.jp'); //ログイン先
  });
});
