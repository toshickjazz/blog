/// <reference types="cypress" />

/**
 * 最初の投稿を取得
 */
function getFirstPost(params) {
  return cy.get('.section-postimgs .postimg').first();
}

context('アドミンモード', () => {
  before(() => {
    // try {
    //   cy.exec('rm ./cypress/screenshots/blog/screenshot.spec.js/screenshot-top.png');
    //   cy.exec('rm ./cypress/screenshots/blog/screenshot.spec.js/screenshot-top-admin.png');
    // } catch (error) {
    //   console.log('削除ファイルなかった');
    // }
  });

  it('ショット（admin）', () => {
    cy.visit('http://localhost:3000/#total=10&isadmin=1');
    cy.get('.postimg').its('length').should('gte', 5);
    // expect($div).to.have.length(1);
    // expect(cy.get('.postimg').length).to.be.greaterThan(5);
    cy.wait(500);
    cy.screenshot('screenshot-top-admin', {
      clip: {
        x: 0,
        y: 500,
        width: 1200,
        height: 800,
      },
    });
  });

  it('ショット（admin）', () => {
    cy.visit('http://localhost:3000/#total=10');
    cy.reload();
    cy.get('.postimg').its('length').should('gte', 5);
    // expect($div).to.have.length(1);
    // expect(cy.get('.postimg').length).to.be.greaterThan(5);
    // cy.wait(500);
    cy.screenshot('screenshot-top', {
      clip: {
        x: 0,
        y: 500,
        width: 1200,
        height: 800,
      },
    });
  });
});
