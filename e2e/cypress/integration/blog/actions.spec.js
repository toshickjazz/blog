/// <reference types="cypress" />

/**
 * 最初の投稿を取得
 */
function getFirstPost(params) {
  return cy.get('.section-postimgs .postimg').first();
}

context('アドミンモード', () => {
  before(() => {
    cy.visit('http://localhost:3000/#total=30&isadmin=1');
  });

  it('削除ボタン出現', () => {
    expect(getFirstPost().find('.postimg-date .dropdown')).to.exist;
  });

  it('コメントUI出現', () => {
    getFirstPost().find('.postimg-comment .btn-edit').click();
    // もう一度参照取得しないとダメ
    expect(getFirstPost().find('.postimg-edit')).to.exist;
  });

  it('コメントUI閉じる', () => {
    expect(cy.get('.postimg-edit .card-header .btn-cancel')).to.exist;
    cy.get('.postimg-edit .card-header .btn-cancel').click();
    cy.notExist('.postimg-edit');
  });
});
