# blog

としっくドキュメントです

## 準備

```
//mkdocsのインストール
pip install mkdocs
```

## 開発方法

```
//ローカルサーバ起動
make dserver
```

http://127.0.0.1:8000



## デプロイ方法

master に push すると ci が走ってドキュメントがデプロイされます

### 仕組み

- push する
- ci が走る
- make docs がコールされて、ルートに public ディレクトリ が生成される

https://toshickjazz.gitlab.io/blog