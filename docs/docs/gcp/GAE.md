# GAE（Google App Engine）

_やってみる_

## Go アプリを用意する

## gcp 上にアプリケーションを作成する

```
gcloud app create
```

## アプリケーションをデプロイする

```
gcloud app deploy
```

<img class="img is-transparent" src="/img/chunamu_400x400.png" width="40" >
