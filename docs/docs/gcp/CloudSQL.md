# Google Cloud SQL

## シークレット

シークレットを作成して PostgreSQL のユーザー名とパスワードをデータベースに提供する

```
//シークレット生成（name: credentials-postgres）
kubectl create secret generic credentials-postgres --from-env-file ./k8s/credentials.txt

//credentials.txt
username=jane
password=d7xnNss7EGCFZusG

//ファイルではなく直接生成
kubectl create secret generic credentials-postgres --from-literal user=***** --from-literal password=*******

//シークレット確認
kubectl get secret credentials-postgres -o yaml

```

## Cloud SQL Proxy を開始

```
//プロキシをダウンロードして権限付与
curl -o cloud_sql_proxy https://dl.google.com/cloudsql/cloud_sql_proxy.darwin.amd64
chmod +x cloud_sql_proxy

//Cloud SQL インスタンスの接続名表示（GCPのGUIからでもとれる）
gcloud sql instances describe [instance name]

//Cloud SQL Proxy を開始
./cloud_sql_proxy -instances="[YOUR_INSTANCE_CONNECTION_NAME]"=tcp:3306
```

<img class="img is-transparent" src="/img/chunamu_400x400.png" width="40" >
