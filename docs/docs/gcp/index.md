# Google Cloud Platform

_AWS みたいなやつ_

## リージョンとゾーン

・リージョン  
地域：東アジア、セントラル US など  
その中にゾーンがある

・ゾーン  
asia-east1-a こんなやつ

## gcloud

```
//現在のプロジェクトIDを出力
gcloud config get-value project

//プロジェクトリスト
gcloud projects list

//GAEのログをみる
gcloud app logs tail -s default

//GAEのアプリをブラウザでひらく
gcloud app browse

//プロジェクト指定して実行
gcloud --project [project id]

//デフォルトプロジェクトの設定
gcloud config set project [project name]

//デフォルトプロジェクト確認
gcloud config list

//サービスアカウント一覧
gcloud iam service-accounts list
```

## gce

```
//gceインスタンス一覧
gcloud compute instances list
```

## gcs

