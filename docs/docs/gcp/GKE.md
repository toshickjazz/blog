# GKE（Google Kubernetes Engine）

## 基本

・クラスターの作成  
・デプロイメントの作成  
・サービスの作成

## デプロイメントとは

pod を管理するコントローラのひとつ

> ・Deployment  
> ・DtatefulSet  
> ・DaemonSet

## ラベルが管理するデータ

・Pod などのオブジェクトに割り当てられたキーとバリューのペア  
・実際よくわからん

> ・"environment" : "dev", "environment" : "qa", "environment" : "production"  
> ・"tier" : "frontend", "tier" : "backend", "tier" : "cache"

推奨ラベル

| キー                         | 説明                                                               | 例                                                       | 型     |
| ---------------------------- | ------------------------------------------------------------------ | -------------------------------------------------------- | ------ |
| app.kubernetes.io/name       | アプリケーション名                                                 | mysql                                                    | 文字列 |
| app.kubernetes.io/instance   | アプリケーションのインスタンスを特定するための固有名               | wordpress-abcxzy                                         | 文字列 |
| app.kubernetes.io/version    | アプリケーションの現在のバージョン                                 | (例: セマンティックバージョン、リビジョンのハッシュなど) | 5.7.21 | 文字列 |
| app.kubernetes.io/component  | アーキテクチャ内のコンポーネント                                   | database                                                 | 文字列 |
| app.kubernetes.io/part-of    | このアプリケーションによって構成される上位レベルのアプリケーション | wordpress                                                | 文字列 |
| app.kubernetes.io/managed-by | このアプリケーションの操作を管理するために使われているツール       | helm                                                     | 文字列 |

## アノテーションが管理するデータ

ビルド、リリースやタイムスタンプのようなイメージの情報、リリース ID、git のブランチ、PR 番号、イメージハッシュ、レジストリアドレスなど

## Kubernetes コマンド

```
//クラスタ情報
kubectl cluster-info

//操作対象を確認
kubectl config current-context

//kubectl の操作対象にする
gcloud container clusters get-credentials [cluster name]

//クラスタ作成（ノード2つ）
gcloud container clusters create --num-nodes=2 [name] --enable-autoscaling --min-nodes=2 --max-nodes=3

（その他オプション）
--zone
--machine-type

//全部削除
kubectl delete deployment,service,pod --all

//クラスタ削除
gcloud container clusters delete [cluster name]

//クラスタ一覧
gcloud container clusters list
```

## container registry コマンド

```
//レジストリにイメージをpush
gcloud docker -- push [image name]:[tag name]

//レジストリからイメージを削除
gcloud container images delete [image name]:[tag name]
```

## デプロイ

まずイメージを作成してそれを Container Registry にアップロード  
そこからデプロイメントをつくって pod を生成  
さいごにそれを外部からアクセスできるようにして完了

```

//Container Registry を認証するには、gcloud を Docker 認証ヘルパーとして使用します。
//プロジェクト リソースに安全で短期間のアクセス権が付与されます
gcloud auth configure-docker

//プロジェクト名を変数に入れよう
GCP_PROJECT=$(gcloud config get-value project)

//ビルド、イメージ作成
docker build -t asia.gcr.io/$GCP_PROJECT/[container name]:[tag name] .

//イメージをContainer Registryにアップロードする
gcloud docker -- push [image name]:[tag name]

//Container Registryを確認する
https://console.cloud.google.com/gcr

//デプロイメントを作成する（deployment nameはここでつける）
kubectl run [deployment name] --image=[image name]:[tag name] --replicas=1 --port=3000 --command -- [command]

//podを確認するとなんかある（デプロイメントとpodの関係が不明）
kubectl get pod
> NAME                                 READY   STATUS    RESTARTS   AGE
> poinfo-deployment-7cd4fb59d5-w8h52   1/1     Running   0          2m4s

//Serviceを作成し、ロードバランサでPodを外部に公開（port：外部接続用 target-port：内部コンテナ用）
kubectl expose deployment [deployment name] --port=80 --target-port=3000 --type=LoadBalancer
> service/poinfo-deployment exposed

これでserviceが生成される

//EXTERNAL-IPが見えていれば公開完了
kubectl get service
> NAME                TYPE           CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
> poinfo-deployment   LoadBalancer   10.51.243.20   34.84.9.250   80:30310/TCP   49s

//デプロイメントのリスト
kubectl get deployments

//デプロイメントの削除
kubectl delete deployment [deployment name]

//サービスの削除
kubectl delete service [service name]
```

# Kubernetes チュートリアルメモ

## Redis のマスターポッドをつくる

```bash
git clone https://github.com/kubernetes/examples
cd examples/guestbook

//gcloud と kubectl の認証情報を設定する
gcloud container clusters get-credentials poinfo-cluster --zone asia-northeast1-a

//コントローラの確認
cat redis-master-deployment.yaml

apiVersion: apps/v1 #  for k8s versions before 1.9.0 use apps/v1beta2  and before 1.8.0 use extensions/v1beta1
kind: Deployment
metadata:
  name: redis-master
spec:
  selector:
    matchLabels:
      app: redis
      role: master
      tier: backend
  replicas: 1
  template:
    metadata:
      labels:
        app: redis
        role: master
        tier: backend
    spec:
      containers:
      - name: master
        image: k8s.gcr.io/redis:e2e  # or just image: redis
        resources:
          requests:
            cpu: 100m
            memory: 100Mi
        ports:


//マスター コントローラをデプロイする
kubectl create -f redis-master-deployment.yaml
> deployment.apps/redis-master created

//実行中のポッドを表示する
kubectl get pods
> NAME                           READY   STATUS    RESTARTS   AGE
> redis-master-596696dd4-92d5x   1/1     Running   0          25m
```

## redis-master サービスの作成

トラフィックを Redis マスターポッドにプロキシするサービスを作成する

```bash
//サービス構成を表示
cat redis-master-service.yaml

apiVersion: v1
kind: Service
metadata:
  name: redis-master
  labels:
    app: redis
    role: master
    tier: backend
spec:
  ports:
  - port: 6379
    targetPort: 6379
  selector:
    app: redis
    role: master
    tier: backend

//サービスを作成
kubectl create -f redis-master-service.yaml
> service/redis-master created

//サービスを確認
kubectl get service
> NAME           TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)    AGE
> kubernetes     ClusterIP   10.84.0.1    <none>        443/TCP    51m
> redis-master   ClusterIP   10.84.6.26   <none>        6379/TCP   7m20s
```

## Redis ワーカーの設定

```bash
//Redis ワーカーの 2 つのレプリカを定義するマニフェスト ファイルを表示
cat redis-slave-deployment.yaml

apiVersion: apps/v1 #  for k8s versions before 1.9.0 use apps/v1beta2  and before 1.8.0 use extensions/v1beta1
kind: Deployment
metadata:
  name: redis-slave
spec:
  selector:
    matchLabels:
      app: redis
      role: slave
      tier: backend
  replicas: 2
  template:
    metadata:
      labels:
        app: redis
        role: slave
        tier: backend
    spec:
      containers:
      - name: slave
        image: gcr.io/google_samples/gb-redisslave:v1
        resources:
          requests:
            cpu: 100m
            memory: 100Mi

//コンテナ クラスタで 2 つのレプリカを起動
kubectl create -f redis-slave-deployment.yaml
> deployment.apps/redis-slave created

//ポッドのリストを照会して、2 つの Redis ワーカー レプリカが実行されていることを確認
kubectl get pods
> NAME                           READY   STATUS    RESTARTS   AGE
> redis-master-596696dd4-92d5x   1/1     Running   0          44m
> redis-slave-96685cfdb-4j2lr    1/1     Running   0          45s
> redis-slave-96685cfdb-jcf6l    1/1     Running   0          45s
```

## Redis ワーカー サービスの作成

・アプリケーションは Redis ワーカーと通信する必要があある  
・Redis ワーカーを検出可能にするには、サービスを設定する必要がある  
・サービスは、一連のポッドに対して透過的な負荷分散を提供する

```bash
//ワーカー サービスを定義する構成ファイルを表示
cat redis-slave-service.yaml

apiVersion: v1
kind: Service
metadata:
  name: redis-slave
  labels:
    app: redis
    role: slave
    tier: backend
spec:
  ports:
  - port: 6379
  selector: #前のステップで作成した Redis ワーカーポッドと一致することに注意
    app: redis
    role: slave
    tier: backend

//サービスを作成する
kubectl create -f redis-slave-service.yaml
> service/redis-slave created

//サービスが作成されたことを確認
kubectl get service
> NAME           TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)    AGE
> kubernetes     ClusterIP   10.84.0.1     <none>        443/TCP    67m
> redis-master   ClusterIP   10.84.6.26    <none>        6379/TCP   23m
> redis-slave    ClusterIP   10.84.11.54   <none>        6379/TCP   96s

```

## フロントエンドの設定

・Redis ワーカーと同様、これはデプロイメントによって管理される複製されたアプリケーション

```bash
//フロントエンド デプロイメントを作成
kubectl create -f frontend-deployment.yaml
> deployment.apps/frontend created

//中身
apiVersion: apps/v1 #  for k8s versions before 1.9.0 use apps/v1beta2  and before 1.8.0 use extensions/v1beta1
kind: Deployment
metadata:
  name: frontend
spec:
  selector:
    matchLabels:
      app: guestbook
      tier: frontend
  replicas: 3
  template:
    metadata:
      labels:
        app: guestbook
        tier: frontend
    spec:
      containers:
      - name: php-redis
        image: gcr.io/google-samples/gb-frontend:v4
        resources:
          requests:
            cpu: 100m
            memory: 100Mi
        env:
        - name: GET_HOSTS_FROM
          value: dns


//確認してみる
kubectl get pods
> NAME                           READY   STATUS    RESTARTS   AGE
> frontend-69859f6796-6dt99      1/1     Running   0          111s
> frontend-69859f6796-jpng8      1/1     Running   0          111s
> frontend-69859f6796-s9bbl      1/1     Running   0          111s
> redis-master-596696dd4-92d5x   1/1     Running   0          56m
> redis-slave-96685cfdb-4j2lr    1/1     Running   0          13m
> redis-slave-96685cfdb-jcf6l    1/1     Running   0          13m
```

## frontend サービスを作成する

```bash
//サービス作成用ファイル
//このファイルの type: NodePort を　type: LoadBalancer に変更してから実行する
frontend-service.yaml


apiVersion: v1
kind: Service
metadata:
  name: frontend
  labels:
    app: guestbook
    tier: frontend
spec:
  # comment or delete the following line if you want to use a LoadBalancer
  type: NodePort #これをLoadBalancerに変更する
  # if your cluster supports it, uncomment the following to automatically create
  # an external load-balanced IP for the frontend service.
  # type: LoadBalancer
  ports:
  - port: 80
  selector:
    app: guestbook
    tier: frontend

//実行
kubectl create -f frontend-service.yaml
> service/frontend created

//確認
kubectl get service
> NAME           TYPE           CLUSTER-IP    EXTERNAL-IP   PORT(S)        AGE
> frontend       LoadBalancer   10.84.7.7     <pending>     80:30061/TCP   25s
> kubernetes     ClusterIP      10.84.0.1     <none>        443/TCP        81m
> redis-master   ClusterIP      10.84.6.26    <none>        6379/TCP       37m
> redis-slave    ClusterIP      10.84.11.54   <none>        6379/TCP       15m

```

<img class="img is-transparent" src="/img/chunamu_400x400.png" width="40" >
