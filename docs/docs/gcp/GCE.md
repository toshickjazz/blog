# GCE（Google Compute Engine）

_やってみる_

自分のプロジェクトの _Google Container Registry API_ を有効にしておくこと

## 指定のパス付きイメージを作成する

```bash
//既存イメージからGCE用イメージを作成する
docker tag [image_name] gcr.io/[project_id]/[name]

//push
gcloud docker -- push gcr.io/[project_id]/[name]
```

GCE のコンソールから「VM インスタンス」を作成する

・「この VM インスタンスにコンテナ イメージをデプロイします。」をチェックする  
・「HTTP トラフィックを許可する」をチェックする

インスタンスグループを作成する

・VM インスタンスと同じ設定をテンプレートに対して行う
