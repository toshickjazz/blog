# Vue テスト

### jestでtsをセットして以下のエラーがでたら

```
Cannot find name 'jest'.
```

tsconfig.jsonの*compilerOptions/types*に"@types/jest"を追加する

```
{
  "compilerOptions": {
    "target": "es2018",
    "module": "esnext",
    "moduleResolution": "node",
    "lib": ["esnext", "esnext.asynciterable", "dom"],
    "esModuleInterop": true,
    "allowJs": true,
    "sourceMap": true,
    "strict": true,
    "noEmit": true,
    "experimentalDecorators": true,
    "baseUrl": ".",
    "paths": {
      "~/*": ["./*"],
      "@/*": ["./*"]
    },
    "types": ["@types/node", "@nuxt/types", "./src/types", "@types/jest"], <-- 追加
    "resolveJsonModule": true
  },
  "exclude": ["node_modules", ".nuxt", "dist", "functions", "stories", "**/*.spec.ts"]
}

```

次に `ts-shim.d.ts` というファイルをtestディレクトリ に配置した。（場所はどこでもよさそう）

```
//ts-shim.d.ts
declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}
```

これでやっとjestのtsファイルからvueファイルを読み込めるようになったのだ。




