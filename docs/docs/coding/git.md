# Git いろいろ

## Git submodule

サブモジュールを追加すると、.gitmodules ファイルが生成されそこにモジュール情報が追加される

```bash
//.gitmodules
[submodule "bootstrap"]
  path = bootstrap
  url = https://github.com/twbs/bootstrap.git
```

### コマンド

```
//サブモジュールを追加する（submodulesディレクトリ を指定）
git submodule add https://github.com/twbs/bootstrap.git submodules/bootstrap

//サブモジュールが参照するコミットIDを出力
git submodule

//パッケージを再現する（cloneが実行される）
git submodule update -i
```

### サブモジュール削除

```
//参照を削除する（.gitmodulesからは消えないしファイルもある）
git submodule deinit -f submodules/bootstrap

//.gitmodulesから削除、cloneしたファイルも消える
git rm submodules/bootstrap

//次にgitディレクトリ から手動削除する（これをする必要があるのか不明）
rm -rf .git/modules/追加したサブモジュール
```

### プロジェクトから見る利用するサブモジュールの更新手順

submodule vue-form を最新の master にしたい

１、checkout した submodule ディレクトリ にて git pull する

2、するとプロジェクトの git には差分が発生する

```
modified:   submodules/vue-form (new commits)
```

３、差分を add して commit すればよい

> プロジェクトのリビジョンを変更したときに submodules がそのときのバージョンではない可能性あり  
> 勝手に追随して submodule も変わってくれるわけではないため

そのときは `git submodule update` を実行すればそのバージョンになる
