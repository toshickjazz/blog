# Vue メモ

## nuxt を Docker から起動してみる

docker-compose.yml を生成

```bash
//docker-compose.yml
version: '3'
services:
  nuxtweb:
    container_name: nuxtweb
    image: node:13.8.0-stretch-slim
    ports:
      - 127.0.0.1:3002:3000
    volumes:
      - .:/app
    stdin_open: true
    tty: true
    working_dir: /app
    command: >
      npm run dev
```

Docker 内で起動した nuxt にホストからアクセスするために package.json を変更

```
//package.jsonにてHOST 0.0.0.0をセットする
"scripts": { "dev": "HOST=0.0.0.0 nuxt", ... }
```

package.json のあるディレクトリ にて上記 docker-compose.yml を配置したあと

```
docker-compose up
```

をすると _http://localhost:3002/_ にてインデックス画面が見える。

docker 内部としては /app に nuxt のトップディレクトリ が展開されている状態
