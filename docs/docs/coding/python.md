# Python

## pyenv

```
// インストール済みのpythonを出力
pyenv versions
```

## システムコマンドをコールする

//指定のディレクトリ から htm ファイルの一覧を取得するよ

```
res = ""
try:
    cmd = 'find ../public -name *.html'
    res = subprocess.check_output(
        'find ../public -name "*.html"', shell=True, universal_newlines=True)
except:
    print("Error.")
```

execSync の戻り値から toString をコールするのがポイントだな

## ファイル読んだり書いたりする

```
// 文字を置換して上書きしよう

let html = fs.readFileSync(url, 'utf-8');

html = html.replace(/src="\/img\//g, 'src="/blog/img/');

fs.writeFileSync(url, html);
```

## html の dom 操作

PyQuery がよい

```
//stringから読み込む（urlからも可能）
pq = PyQuery(html, parser='html')

//html全体を出力する場合 html()だとダメ
html = pq.outerHtml()
```
