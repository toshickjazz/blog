# PostgreSQL

## PostgreSQL にログイン

```bash
psql -h 127.0.0.1 -p 5432 -U postgres -d postgres

//こーいう書き方もあるよーだ
psql 'postgres://<username>:<password>@<host>:<port>/<database>?sslmode=require'
```

## 基本

```
//DB一覧
\l

//DB作成
CREATE DATABASE ronten;

//DB接続
\c ronten


```

## role

postgres にログインしたあと以下を実行

```
//role一覧を出力
\du

//role を作成
create role camaleao with login password createdb createrole 'camaleao'

//role を削除
drop role camaleao
```

## goose in Makefile

```bash
#
# goose_prd
#
.PHONY: goose_prd
goose_up_prd:
  goose -dir db/migration postgres "user=postgres dbname=myao sslmode=disable password=root host=camaleao-postgres" up

goose_down_prd:
  goose -dir db/migration postgres "user=postgres dbname=myao sslmode=disable password=root host=camaleao-postgres" down

goose_st_prd:
  goose -dir db/migration postgres "user=postgres dbname=myao sslmode=disable password=root host=camaleao-postgres" status
```

コンテナ間通信を行う場合（prd）は、host のところにコンテナ名をセットすればよい

## ヒアドキュメント postgres 実行

```bash
#!/bin/sh

//これでパスワードセットできる
PGPASSWORD=root psql -U postgres -h 127.0.0.1 -p 5432 -d postgres <<_EOD

create database camaleaopay;

_EOD
```
