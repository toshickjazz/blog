# Mysql

```
//user root, password: root でログイン
mysql -u root -proot

//データベース一覧
show databases;

//データベースを選択
user DBNAME;
```

## mysql にログイン

```bash
mysql -uroot -p -P 3306 -h 127.0.0.1
```

ユーザ: root  
パスワード：対話形式で聞いてね  
ポート: 3306  
ホスト: ホストマシンアドレスを指定

## ローカル docker にて mysql

```
docker run -it --name mysql -e MYSQL_ROOT_PASSWORD=root mysql:latest /bin/bash
```

## 現在のテーブル情報を表示する

```
SHOW PROCESSLIST;
```

## goose in Makefile

```bash
.PHONY: goose
goose_up:
  goose -dir db/migration mysql "root:root@tcp(db:3306)/myao?parseTime=true" up

goose_down:
  goose -dir db/migration mysql "root:root@tcp(db:3306)/myao?parseTime=true" down

goose_st:
  goose -dir db/migration mysql "root:root@tcp(db:3306)/myao?parseTime=true" status

.PHONY: db
dump:
  mysqldump --single-transaction -uroot -proot -h 127.0.0.1 myao > myao.dump

restore:
  mysql -uroot -proot -h 127.0.0.1 myao < myao.dump

```

tcp(db:3306)のところはコンテナ名でもいけるかもしれない
