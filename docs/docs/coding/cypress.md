# Cypress メモ

```
//属性の存在チェック
expect(btn.attributes().ddd).toBeFalsy();

//domの存在チェック
cy.get(selector).should('exist');
cy.get(selector).should('not.exist');

//n個以上あるか
cy.get('.postimg').its('length').should('gte', 5);

```

## Tips

```
//textInput.setValue(value) は以下のエイリアス
textInput.element.value = value
textInput.trigger('input')
```
