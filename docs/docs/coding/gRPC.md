# gRPC

## gRPC-Web

REST の代わりになる通信プロトコル  
Envoy を経由すれば gRPC プロトコルにのっかった http 通信でサーバとやりとりできる。  
型安全な世界で  
つまり front からは直接 gRPC プロトコルによるサーバとの通信は不可能

<img class="img" src="/img/grpc01.png" >

## セットアップ

```
//Protocol Buffersをコンパイルするためのコマンド
brew install protobuf

//npm
npm i -D grpc-tools grpc_tools_node_protoc_ts
```

## proto ファイル

ここにインタフェースを定義する

```

syntax = "proto3";

package test_user;

//getUsersがエントリポイント
service UserDomain {
    rpc getUsers (UsersRequest) returns (UsersReply);
}

```
