# CSS について

## postcss-custom-properties

sass を使わずにグローバル変数を定義、使用できるようになる

```css
:root {
  --mainColor: #59bb0c;
}

.Logo {
  fill: var(--mainColor);
}
```

## postcss-custom-media

sass を使わずにカスタムメディアクエリを定義、使用できるようになる

```
@custom-media --mobile (max-width: 640px);

@media (--mobile) {
  .selector {
      property: value;
  }
}
```

## CSS のルールを考える

正解のない css ルールだが、RSCSS をベースとした設計を採用することにした。  
シンプルなのがよい。

## コンポーネント（BEM の Block）

userpanel という名前のユーザ画像とユーザ名を含んだパネル状のコンポーネントがある。

<ul class="list-userpanel">
  <li>
    <div class="userpanel">
      <div class="userpanel-img">
        <img  src="/img/main.png" >
      </div>
      <div class="userpanel-name">せかいのとしっく</div>
    </div>
  </li>
  <li>
    <div class="userpanel -red">
      <div class="userpanel-img">
        <img  src="/img/main.png" >
      </div>
      <div class="userpanel-name">せかいのとしっく</div>
    </div>
  </li>
  <li class="_margin-left-25">
    <div class="userpanel">
      <div class="userpanel-img">
        <img  src="/img/main.png" >
      </div>
      <div class="userpanel-name _bold">せかいのとしっく</div>
    </div>
  </li>
</ul>

```html
<ul class="list-userpanel">
  <li>
    //userpanelコンポーネント
    <div class="userpanel">
      <div class="userpanel-img">
        <img src="/img/main.png" />
      </div>
      <div class="userpanel-name">せかいのとしっく</div>
    </div>
  </li>
  <li>
    //赤いバージョン
    <div class="userpanel -red">
      <div class="userpanel-img">
        <img src="/img/main.png" />
      </div>
      <div class="userpanel-name">せかいのとしっく</div>
    </div>
  </li>
  <li class="_margin-left-25">...</li>
</ul>
```

コンポーネントに所属するパーツはハイフンで連結。  
階層はどこにあってもハイフン一つで連結する。

```
//userpanel に所属している画像
.userpanel-img

//userpanel に所属している名前
.userpanel-name
```

userpanel のリストを表すクラスは？

```
<div class="userpanel-list">
または
<div class="list-userpanel">
```

「userpanel-list」の方がわかりやすいが、これだと「userpanel」コンポーネントに所属している list だという解釈が存在する。  
したがってまず「これはリストである」という「list-userpanel」の方を採用する。  
list などという汎用的すぎるコンポーネント名は存在しないためこれでよいと考える。

```comment
今後考えがかわる可能性もあるね
```

## Variant（modifier）

変化（variant）のある場合はハイフンから始める。

```
<div class="userpanel -red">
```

```comment
bulmaの is-XXX よりさらに短くてしかもわかりやすい
```

## ヘルパー

三つ目の li には「\_margin-left-25」というクラスがついている。  
さらにユーザ名には「\_bold」がついている。

```html
<div class="userpanel-name _bold">せかいのとしっく</div>
```

このように、細かく分割された要素への変化クラスはヘルパークラスと呼ぶことにする。  
ヘルパークラスはアンダースコアで開始する。  
ただ、margin などのヘルパーは使わないほうがよい。  
レスポンシブ対応するときなどに付与クラスがどんどん増えていくためである。

```html
<div class="_margin-left-25-desktop _margin-left-10-mobile"></div>
```

これは煩雑。

```comment
ただ、ヘルパーも Variant と同じくハイフンでいいのでは...という気持ちもある
```

## ブロックへの名付け

```
//会社の記事である
<article class="article-company">
  //グローバルヘッダ
  <header class="header-global"></header>

  //ユーザパネル一覧のセクション
  <section class="section-userpanel-list">
    <ul class="list-userpanel">
      <li>
        //これはコンポーネント
        <div class="userpanel">...</div>
      <li>
    </ul>
  </section>

  //会社紹介のセクション
  <section class="section-about-mycompany"></section>

  //グローバルフッタ
  <footer class="footer-global"></footer>

</article>
```

header や section などのタグには「何のヘッダなのか」「何のセクションなのか」を意味するクラスを積極的に付与する。（li などは除外）  
これは e2e テストの際に非常に都合がよい。  
コンポーネントのパーツか、それともブロック名称なのかは最初のワードで判断する。

これはつまり、特段そのセクションに現状名付ける必要性がなくてもつけるべきである。

> このときの付与ルールは「タグ名（または汎用的な意味）-説明」である  
> .section-userpanel-list

```comment
最初のハイフン以降は自由なので、いくらでもハイフンで区切って良い
```

## コンポーネントは汎用的であるべき

コンポーネントは*特定のポジションで使われるべきだという発想は持たない*。  
つまり自分の内部構成要素としての margin、padding 以外は持つべきではない。

> `そのコンポーネントには右側に 20px の空きが必要だ。`  
> という状態は、そのコンポーネントを利用する側が決めるべきである。

```
<div class="myinfo">
  <div class="userpanel">...</div>
</div>

.myinfo .userpanel{
  margin-right: 20px;
}
```

または用途が明確な場合は Variant で解決する

```
<div class="userpanel -fix-top-right">...</div>

.userpanel.-fix-top-right{
  position: 'fixed';
  top: 10px;
  right: 10px;
}
```
