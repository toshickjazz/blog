#Terraform

### 変数

```
variable "aws_region" {
    default = "ap-northeast-1"  # デフォルト値を設定
    region = "${var.aws_region}" #変数展開
}
```

### cliから変数を渡す

```
terraform apply -var 'xxx=123' -var 'yyy=345'
```



### Virtual Private Cloud（VPC）ネットワーク

- VPCを使って、Compute Engine 仮想マシン（VM）インスタンス、Google Kubernetes Engine（GKE）クラスタ、App Engine フレキシブル環境インスタンス、プロジェクト内のその他のリソースへ接続できます

```
resource "aws_vpc" "tf_vpc" {
    cidr_block           = "10.0.0.0/16"
    instance_tenancy     = "default"
    enable_dns_support   = true
    enable_dns_hostnames = true
    tags {
        Name = "tf_vpc"
    }
}
```

### コマンド

```
terraform plan //dryrun
terraform apply //適用
terraform destroy //削除

terraform remote push //gsc等にtfファイルをpushして共有して使うためのコマンド
```

## （gcp）サービスアカウント発行 from cli

```
//プロジェクト設定
gcloud config set project [PROJECT_ID]

//発行
gcloud iam service-accounts create terraform-serviceaccount --display-name "Account for Terraform"
```

