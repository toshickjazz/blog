# Golang

## go mod を使おう

Go の 1.11 から標準で使えるようになった go mod をつかう

### 環境変数をセットする必要あり

```
export GO111MODULE=on
```

### よく使うコマンド

```
//初期化
go mod init [PATH]

//既存パッケージを出力
go list -m all

//パッケージ追加
go get

//利用していないパッケージを削除
go mod tidy

//go.modからパッケージをインストール
go mod download

//全goファイルからimportしている全パッケージをインストール
go get -u -t ./...
```

## go mod init [PATH]

PATH は省略すると自分のパスが go.mod に追加される

```
//やってみる
go build app/main.go
```

↓ go build 実行前 go.mod

```bash
module gitlab.com/camaleao-pay

go 1.13
```

↓ go build 実行後 go.mod

```bash
module gitlab.com/camaleao-pay

go 1.13

require (
  github.com/go-playground/universal-translator v0.17.0 // indirect
  github.com/gorilla/sessions v1.2.0 // indirect
  github.com/labstack/echo v3.3.10+incompatible
  github.com/labstack/echo-contrib v0.8.0 // indirect
  github.com/leodido/go-urn v1.2.0 // indirect
  gopkg.in/go-playground/validator.v9 v9.31.0
)
```

## 依存パッケージのインストール

```
go mod download
```

これを実行すると、GOPATH/pkg/mod ディレクトリ にパッケージがインストールされる

## vendor

```
go mod vendor
```

これを実行すると アプリディレクトリ /vendor に依存パッケージがインストールされる

> 開発時 -> mac の gopath にインストールされているので OK  
> 本番ビルド時 -> go mod download にて docker の gopath にインストールされるので OK  
> go mod vendor しておくと、./vendor ディレクトリ に入っているのでインストール実行は不要

依存パッケージが public ならば docker 上からインストールが可能だが、認証があり docker 上からインストール実行がめんどい場合は vendor をつかっておくのが無難とのこと  
その場合は vendor ディレクトリ を出力して git 管理しておくことになる

## handler

ルーティングの処理では何をどう処理しているのか

```golang
//HandleFuncにてhttpにルーティング処理をセットする
http.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
  fmt.Fprintf(w, "Hello World")
})

//DefaultServeMuxによりルーティングを処理する
http.ListenAndServe(":8080", nil)

```

http パッケージには Handler インタフェースが定義されている

```golang
type Handler interface {
    ServeHTTP(ResponseWriter, *Request)
}
```

この ServeHTTP 関数が用意されている構造体は全て Handler である

### http.Handle メソッド

```golang
//単にHandle処理をDefaultServeMuxにやらせるためのラップ関数
func Handle(pattern string, handler Handler) { DefaultServeMux.Handle(pattern, handler) }
```

DefaultServeMux.Handle はパターンと処理関数を紐づける

### http.HandlerFunc 型

```
type HandlerFunc func(ResponseWriter, *Request)
func (f HandlerFunc) ServeHTTP(w ResponseWriter, r *Request) {
  f(w, r)
}
```

ServeHTTP 関数が用意されているので Handler として扱える。  
実際に ServeHTTP がコールされたとき、自分を実行して ResponseWriter と Request を処理する  
この型にキャストすると、なんと ServeHTTP メソッドを強制付与することができる

```
//キャストするとServeHTTPメソッドが生える
HandlerFunc(func(ResponseWriter, *http.Request))

//ミドルウェアでフィルター処理をする（http.Handlerを引数として受け取り、さらにhttp.Handlerを返すのがミドルウェア）
func filter(next http.Handler) http.Handler {
  // 一見ただの関数をhandlerとして返している。handlerにはServeHTTPメソッドが必要なはず...
  // つまりこの無名関数はコールバックではなくキャストである。
  // HandlerFuncによるキャストがServeHTTPメソッドを追加しているのだ。
  return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
    //前処理
    next(w, r) // ラップする対象のhttp.Handler
    //後処理
  })
}
```

## コンテキスト

Google ではリクエストスコープな値とかキャンセルシグナル,デッドラインやらをリクエストに関係する全てのゴルーチンに渡せる context パッケージを開発したよ

### 守られるべきルール

> ・構造体の中には、Context を保持してはならない。Context が必要な関数には明示的に渡す事。Context は第１引数であるべきで、だいたいは ctx と名付けてるよ。  
> ・関数側が許容するとしても、nil の Context を渡してはいけない。どのコンテキスト渡して良いか確証が持てない時は、Context.TODO を使って空の Context を渡してね。  
> ・Context の Value は API やプロセスをまたぐリクエストスコープな値だけに使う。オプショナルな値を関数に渡すためではない。  
> ・同じ Context は別々に実行されている goroutine で関数渡しても良い。Context は複数の goroutine から同時に使われても安全である。

```
//基本これで生成する
ctx := context.Background()

//不要な場合でもnilのかわりにこれで生成したものを渡す
ctx := context.TODO()

//リクエストオブジェクトから取り出す
ctx := http.Request.Context()

//キーバリューストアとしても動作
ctx = context.WithValue(ctx,key,val)
val := ctx.Value(key)
```

### キャンセル処理

```
//キャンセル関数を生成する（子のgoroutineプロセスのキャンセルを制御したい場合に使用する）
childCtx, cancel := context.WithCancel(ctx)

//一定時間後にキャンセルを実行する
childCtx, cancel := context.WithTimeout(ctx, time.Second*10)
```

## gRPC

RPC (Remote Procedure Call) の一つ。 高速な通信を実現。  
API 仕様を IDL（インターフェース定義言語）として .proto ファイルに定義する。  
IDL のおかげでプログラム言語の多くに対応できる。

> .proto ファイル -> コンパイル -> 各言語のファイルを出力する

```
.proto ファイル

// HelloRequest を受け取って HelloReply を返すメソッドの定義
service Greeter {
  rpc SayHello (HelloRequest) returns (HelloReply) {}
}

// HelloRequest のリクエスト定義
message HelloRequest {
  string name = 1;
}

// HelloReply のレスポンス定義
message HelloReply {
  string message = 1;
}
```

### REST Api のかわりになるのか

・grpc-gateway というサーバを立てて、リクエストに来た json を gRPC に変換する（リバースプロキシ）  
・これも proto から自動生成可能  
・front は通常どおり REST api をコールすることになる  
・バックエンド的には リクエストの json に対応する型を書く手間が減るようだ
