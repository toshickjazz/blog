# CircleCI

## CircleCI

### version

2 か 2.1 を指定

## jobs 系

複数の job を指定する

jobs -> [build|lint|deploy|test]  
または自作の executors をセットする

### jobs-build

### _jobs -> build -> steps_

コマンドなど指定する。ビルド環境に依存しないもの？

### _jobs -> build -> docker_

docker を利用する。auth（認証情報）や image を指定する  
docker の他に machine、macos がある

### _jobs -> build -> parallelism_

並列実行するコンテナ数

### _jobs -> build -> working_directory_

実行ディレクトリ （default: ~/project）

## steps 系

### _steps -> checkout_

git リポジトリを working_deirectory の場所に checkout する

### _steps -> run, name, command_

コマンド実行

条件判定もできる  
when, unless, condition で判定し、steps で実行させる

### _steps -> restore_cache, key, save_cache_

ファイルをキャッシュして時間短縮させる

### _steps -> store_articacts_

バイナリを s3 にアップロード（3GB まで）

### _steps -> persist_to_workspace, attach_workspace_

job 間でファイルを共有するための設定

## commands 系

関数のように再利用処理を定義できる

### _commands -> [command name] -> description,paraneters,steps_

コマンド名を定義して、説明、パラメータ、steps を定義

## executors 系

再利用可能な実行環境（build 設定）を定義する  
docker、machine、macos がある。利用 image をセットする。

### _executors -> [myexecutors] -> docker_

- myexecutors を定義する
- jobs -> [自作 executors 名] -> executor, steps として jobs で指定する

## workflows 系

ビルドパイプライン  
パラレル、シーケンシャルに実行を指定できる  
requires 指定による、ファンイン・ファンアウトの実行も可能  
下記を順番に実行させる

・ビルド job（1 つ）-> テスト job（4 つ）-> デプロイ job（1 つ）  
・ビルドの完了をトリガにテスト job4 つがパラレルに開始される  
・テスト job4 つの全完了をトリガにデプロイジョブが開始される  
・workflow ステータス

```bash
RUNNING: 進行中
NOT RUN: 開始されなかった
CANCELLED: 進行途中でキャンセルされた
FAILING: 進行途中で workflow 内のある job が失敗している
FAILED: workflow 内の 1 つ以上の job が失敗した
SUCCESS: workflow 内の全ての job が成功した
ON HOLD: workflow 内のある job が承認待ち
NEEDS SETUP: config.yml に文法的な誤りがある
```

## triggers 系

実行トリガを設定。デフォルトでは branch push
cron での設定も可能

### _triggers -> filter_

branch 名、tag 名による実行条件を指定
ignore、only などでフィルタできる

## 承認系（ON HOLD）

### _jobs -> type: approval_

webui 上でのボタンクリックによる承認を必須とする

# ローカルで実行する

circleci の cli をインストール

```bash
\$ curl https://raw.githubusercontent.com/CircleCI-Public/circleci-cli/master/install.sh --fail --show-error |bash

//config.yml を作成
vi .circleci/config.yml

//コンフィグの正当性チェック
circleci config validate

//実行
circleci local execute
```


