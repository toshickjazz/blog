# npm メモ

## npm レジストリ公開関連

```bash
//ログイン
npm login

//公開ファイルを確認する
npm pack

//公開する
npm publish

//バージョンの更新をする（package.jsonも書き変わる）
npm version patch （v1.0.0 -> v1.0.1）
npm version minor （v1.0.0 -> v1.1.0）
npm version major （v1.0.0 -> v2.0.0）

//更新をする
npm publish
```

## publish 前ビルド

publish するとローカルディレクトリ のそのままの構成で npm リポジトリに publish される

> ・ソースを含めたくない -> publish したくないファイルを.npmignore ファイルに追記する  
> ・必ずビルドされたバイナリファイルを main ファイルにしたい -> prepublishOnly スクリプトにビルド処理をかく

```
package.json

//prepublishOnlyにてpublish前にビルドファイルをpack内に含めることができる
"scripts": {
  "dev": "tsc --watch",
  "test": "jasmine-node test/",
  "build": "rm -rf dist && tsc",
  "prepublishOnly": "npm run build"
},

//利用側がnpmをimportするときのファイルはmainプロパティで指定する
"main": "dist/index.js",
```
