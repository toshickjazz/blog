# HTML について

## 表示（critical rendering path）までの流れ

### １、バイトアレイが届きます

### ２、バイトを文字に変換します

### ３、文字をトークンに解釈します

・何のタグかの解析

### ４、トークンをノードに変換します

### ５、ノードを組み合わせて DOM ツリーを構築します

### ６、css から CSSOM ツリーを構築します

・css のツリー構造です

### ７、DOM ツリーと CSSOM ツリーを合体してレンダーツリーを構築

・ノードに対して css を当てはめて display:none などのノードをレンダーツリーからはずす

### ８、リフローによりレイアウト計算が実行される

・viewport からトップノードをどこに配置すべきかを計算していく

### ９、最後にペイントが実行されて表示完成

```comment
dom ノード変更や css 変更が入ったら、7 からやり直すのだなきっと
```

## エンジン

| ブラウザ        | レンダリングエンジン | js エンジン  |
| --------------- | -------------------- | ------------ |
| Google Chrome   | Blink                | V8           |
| Safari          | Webkit               | Nitro        |
| IE              | Trident              | Chakra       |
| Microsoft Edge  | Blink                | V8           |
| Mozilla Firefox | Gecko                | SpiderMonkey |
| Opera           | Blink                | V8           |


