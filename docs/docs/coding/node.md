# Node いろいろ

## npm、node のバージョン固定

### 方法１　.node-version ファイルを配置すると node バージョンを固定できる

```bash
//.node-version
v10.0.0
```

この方法の場合 `npm ci` のときもエラーを出す  
npm は制限できない

### 方法２　 package.json で node と npm 両方固定する

```bash
//package.jsonにengineを追加する
"engines": {
  "node": "10.10.0",
  "npm": "5.6.0"
},

//.npmrcを追加して以下を入力
engine-strict=true

```

この方法の場合 `npm ci` のときはエラーを出さない
npm も含めて制限可能

## システムコマンドをコールする

```
//指定のディレクトリ からhtmファイルの一覧を取得するよ

const execSync = require('child_process').execSync;
const result = execSync('find ../public -name "*.html"').toString();

execSyncの戻り値からtoStringをコールするのがポイントだな
```

## ファイル読んだり書いたりする

```
// 文字を置換して上書きしよう

let html = fs.readFileSync(url, 'utf-8');

html = html.replace(/src="\/img\//g, 'src="/blog/img/');

fs.writeFileSync(url, html);
```

## nodenv

nodebrew より優秀  
特定のディレクトリ に対して node バージョンをセットできる  
プロジェクト毎切り替え作業が不要となる

```
//マシンのnodeバージョンをセット
nodenv global 13.0.0

//ディレクトリ にnodeバージョンをセット
nodenv local 12.0.0

//セットしたローカルnodeを削除
nodenv local --unset
```
