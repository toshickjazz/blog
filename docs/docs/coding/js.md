# JS いろいろ

## Object.create(null)と{}の違い

いずれも空の Object 生成のための手法だが、ほんの少し違う。

```
//prototypeチェーンがあるオブジェクト
//したがってconstructorメソッドがすでに定義されている
const x = {};

//なんもない空のオブジェクト（引数にはprototypeをセットする。それがnullなのでprototypeなし）
const z = Object.create(null);
```

```comment
現在はMapをつかったほうがよいそうだ
```
