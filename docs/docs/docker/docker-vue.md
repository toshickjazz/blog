# Docker で Vue

## nuxt を Docker から起動してみる

docker-compose.yml を生成

```bash
//docker-compose.yml
version: '3'
services:
  nuxtweb:
    container_name: nuxtweb
    image: node:13.8.0-stretch-slim #Dockerfileを利用せずに直接ベースイメージを指定してコンテナ起動
    ports:
      - 3002:3000 #ホストから要求した3002番の要求をdockerの3000番にリレーする
    volumes:
      - .:/app #自分のディレクトリ をdocker内部の/appディレクトリ としてマウントする
    stdin_open: true
    tty: true
    working_dir: /app #カレントディレクトリ を指定
    command: > #起動時に実行するコマンド
      npm run dev
```

Docker 内で起動した nuxt にホストからアクセスするために package.json を変更

```
//package.jsonにてHOST 0.0.0.0をセットする
"scripts": { "dev": "HOST=0.0.0.0 nuxt", ... }
```

package.json のあるディレクトリ にて上記 docker-compose.yml を配置したあと

```
docker-compose up --build
```

をすると _http://localhost:3002/_ にてインデックス画面が見える。

docker 内部としては /app に nuxt のトップディレクトリ が展開されている状態
