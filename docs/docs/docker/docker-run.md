# Docker コマンド操作

## コンテナ起動には「コマンドから起動」と「docker-compose」からの起動の２種類がある

## コマンドからのコンテナの起動

```
docker run -it -p 8000:80 --name myweb xxx/httpd:tagname /bin/bash
```

- イメージ「xxx/httpd:tagname」からコンテナを起動する
- ポートはホストの 8000 番をコンテナ内の 80 にポートフォワードする
- 起動直後に bash を起動する

### NyanCat してみる

よくわかっていない人も、とりあえず以下を実行してみよう

```
docker run -it --rm supertest2014/nyan
```

NyanCat のイメージをローカルにダウンロードして実行し、終了後にコンテナを自動で削除する  
（ローカルにイメージがない場合にダウンロードが始まる）

## コンテナ侵入

```
//コンテナのbashに接続する
docker exec -it CONTAINER /bin/bash

//これでもいけるみたい
docker-compose exec [SERVICE] sh

（SERVICEはdocker-compose.ymlのサービス名）
```

## 情報表示

```
//イメージ一覧
docker images

//停止コンテナも含めて表示
docker ps -a

//psをフォーマット指定して表示
docker ps —format '{{.ID}}   {{.Names}}   ——  {{.Status}}'

//コンテナ内の実行中プロセス一覧を出力
docker top CONTAINER

```

## リポジトリとイメージの関係

```
docker images
supertest2014/nyan   latest              3dd6111154fc        5 years ago         262MB

//タグを付与してリポジトリを作成する
docker tag supertest2014/nyan supertest2014/nyan:toshick

docker images
supertest2014/nyan   latest              3dd6111154fc        5 years ago         262MB
supertest2014/nyan   toshick             3dd6111154fc        5 years ago         262MB
```

tag コマンドにて生成されるのはリポジトリである。  
指定タグが付与された、同一のイメージを参照しているリポジトリが増えるのである。

### イメージ ID でイメージ削除する

このとき docker rmi の引数にイメージ ID を指定するとどうなるだろうか

```
Error response from daemon: conflict: unable to delete 3dd6111154fc (must be forced) - image is referenced in multiple repositories
```

同じイメージを参照しているリポジトリが複数あるためエラーが出る。  
f オプションによりこれを無視して強制的に削除することができる。

```
docker rmi 3dd6111154fc -f
```

### リポジトリ:tag でリポジトリ削除する（結果ゼロリポジトリならばイメージ削除）

```
//イメージ一覧
docker images
supertest2014/nyan   latest              3dd6111154fc        5 years ago         262MB
supertest2014/nyan   toshick             3dd6111154fc        5 years ago         262MB

//タグtoshickのnyancatリポジトリを削除する
docker rmi supertest2014/nyan:toshick
Untagged: supertest2014/nyan:toshick

//イメージの本体はまだある
docker images
supertest2014/nyan   latest              3dd6111154fc        5 years ago         262MB

```

いかがだろうか。  
つまりリポジトリ指定による rmi コマンドで消えるのは指定タグのリポジトリだけでありその参照元のイメージは消えてない。  
しかし、イメージが参照するリポジトリがゼロになった場合やっとイメージも消えるのである。  
では今度は最後の参照リポジトリであるタグ latest を消してみよう。

```bash
d rmi supertest2014/nyan
Untagged: supertest2014/nyan:latest
Untagged: supertest2014/nyan@sha256:757f8da8d28e0ac8b1ddda3e8082acf94216b5dc719f6df71d92fe87a782648f
Deleted: sha256:3dd6111154fcfa03e36c0ceb9942e4f02befaaab605064dbd702a7724532bad5
Deleted: ...
Deleted: ...
Deleted: ...
...
```

これでイメージも消えた。

> rmi コマンドではリポジトリまたはイメージ ID のどちらかを指定できる。  
> イメージ ID 指定により、そのイメージを参照しているリポジトリ全てを強制削除することができる  
> リポジトリ指定して削除したとき、参照元となるリポジトリがゼロになった場合にはじめてイメージも削除される。

```comment
つまり、docker imagesで出現する一覧ってimege一覧じゃあないね。リポジトリ一覧だよね。
```

## 停止・削除系

```
//イメージ削除
docker rmi IMAGE

実際には指定した

//コンテナ削除
docker rm CONTAINER

//全コンテナ停止
docker stop $(docker ps -a -q)

//全コンテナ削除
docker rm $(docker ps -a -q)

//全ての停止中のコンテナ、ボリューム、ネットワーク、イメージを一括削除する
docker system prune

//使われていないimageを一括削除する
docker image prune

//使われていないcontainerを一括削除する
docker container prune

//<none>イメージを一括削除
docker rmi $(docker images -f 'dangling=true' -q)

//ぜんぶ消す
docker system prune —all

//現在起動中のコンテナに関わるものはそのまま残したい場合
docker system prune -af —volumes

```

## その他

```
//データ格納用ボリュームを作成する
docker volume create —name KycDocsVolume


//コンテナからイメージを作成する
docker commit CONTAINER REPOSITORY[:TAG]

```

## レジストリ（docker hub）

```
//docker hubからgoでイメージを検索する
docker search go

//docker hubからイメージをダウンロードする（tagの指定を省略するとlatestタグが選択される）
docker pull IMAGE

//ログイン
docker login
```

## ネットワーク

```
//ネットワーク一覧
docker network ls

//ネットワーク作成
docker network create my_network

//ネットワーク削除
docker network rm my_network

//ネットワーク情報出力
docker network inspect my_network

```

### 同一ネットワークにいるのかチェック

```
docker network inspect my_network

"Containers": {
  #子の中に接続したい同志のコンテナの情報が入っていればOK
  ....
}
```

## SQLiteをインストールする

```
apt-get update && apt-get install sqlite3 libsqlite3-dev -y
```





<img class="img is-transparent" src="/img/chunamu_400x400.png" width="40" >