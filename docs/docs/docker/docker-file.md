# Dockerfile

_イメージ作成を自動化する_

##Dockerfile からイメージ生成

```
//Dockerfileの場所を自分のディレクトリとしてイメージ作成
docker build -t リポジトリ名:タグ .
```

実行ディレクトリ にある Dockerfile から、xxx/httpd:tagname という名前のイメージを生成する

## 証明書

```
# fuck the netskope
COPY ./cafile.pem /etc/ssl/certs/cafile.pem
RUN update-ca-certificates
```

```comment
Dockerfile は１サービスを生成するためのファイルである。つまり１コンテナ起動。
```

# docker-compose（Composefile）

##複数のイメージ作成を一度に行う

```
services:
  db:
    image: mysql #ベースイメージを指定（Dockerfileは使用しない）
    restart: always #実行時に再起動するかどうか
    environment: #環境変数をセット
      MYSQL_ROOT_PASSWORD: hogehoge
      MYSQL_PORT: 3306 # MySQLのデフォルトポート
    ports:
      - 4306:3306 # 4306のリクエストがきたら、3306へリレーする
    volumes:
      - ./usr/rails/mysql/conf:/etc/mysql/conf.d:rw # Composefileからみた相対パスでmysql/confが呼び出されたら、etc/mysql/conf.dを使ってrw(読み込み・書き込み)を行う。実際は、再ビルド後も、DBの中身がリセットされないようにしてる。
      - ./mysql-datavolume:/var/lib/mysql # Composefileからみた相対パスでmysql-datavolumeが呼び出されたら、/var/lib/mysqlを指定して実行
      - "bundle:/usr/local/bundle" # Composefileからみた相対パスでbundleが呼び出されたら、ローカルのbundleファイルを指定して実行。これにより、ローカルで実行したbundle installの内容が永続化(=変更内容の適用)される。
      - ./sql:/docker-entrypoint-initdb.d # 初期化用に一度だけ実行されるディレクトリ にsqlファイルなどが置いてあるディレクトリをマウントする
  web:
    build: . #Dockerfileのあるディレクトリを指定
    command: bundle exec rails s -p 3000 -b '0.0.0.0' #デフォルトのコマンドを上書きする
    command: > #これで改行できる
      npm run dev
    working_dir: /app #これがあればDockerfileのWORKDIRは不要
    volumes:
      - .:/myapp
    ports:
      - "3000:3000" #この指定があればDockerfileのEXPOSEは不要
    depends_on:
      - db # ここで指定したServiceは、`docker-compose run`した際に、指定元のServiceが実行されるよりも前に呼び出されるようになる。つまり、webというServiceよりも前に、dbというサービスが実行される。
  frontend:
    depends_on:
      - db
    ports:
      - "8200:8200"
    entrypoint: "/bin/sh -c 'npm install && npm run start'" # この記述によって、フロントエンドのフレームワーク最大の恩恵であるlive loadがdockerでも使える。ただし、このComposeFileでentrypointを指定すると、Dockerfile内のCMDやENTRYPOINTは無視されるので注意。
volumes: # このようにトップレベルでvolumesを指定すると、名前付きボリュームになる。
  bundle:
    driver: local # bundleを使う際のdriverとしてlocalを指定
  mysql-datavolume:
```

## service

services 直下にある _db、web、fonrtend_ が service である。  
任意の文字列でよい

> サービスの image を一つつくるためのものが Dockerfile。  
> 複数のサービスを一度に起動するものが docker-compose

サービスの元となるイメージは _build_ プロパティから場所を指定すればそこにある Dockerfile からイメージ生成を行うし、*image*プロパティからベースイメージを直接指定する場合もある。

<img class="img is-transparent" src="/img/chunamu_400x400.png" width="40" >
