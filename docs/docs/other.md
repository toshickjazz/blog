# その他メモ

## VSCode

### Setting Sync（VSCode）

・Setting Sync エクステンションで設定を複数マシンで共通化できる  
・gist に設定をアップロードして行う（エクステンションが実行する）

### sass がフォーマットできません

```
//もしsassのフォーマットで「フォーマットできません」とでた場合、configの以下の行を削除してみよう
"editor.defaultFormatter": "vscode.configuration-editing",
```
