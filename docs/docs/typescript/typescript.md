# TypeScript

## 型をフィルタする（希望のユニオンタイプを得る）

Animal 型を継承した Dog 型、Cat 型、継承していない Influenza 型 の三つの型を持つ Creature 型というユニオン型がある。  
この Creature から Animal 型 だけを抽出して AnimalType という型をつくる

```
//Creatureは複数のタイプのどれかの型（ユニオン型）
type Creature = Dog | Cat | Influenza;

//IsAnimalにユニオン型を渡すと、Animalインタフェースを継承した型だけのユニオン型になる（フィルタされる）
type IsAnimal<T> = T extends Animal ? T : never;

//IsAnimalにCreatureをわたすと、Dog | Cat のユニオン型が得られる
type AnimalType = IsAnimal<Creature>;
```

## infer を使うと構文中に型をキャプチャできる

```
//Code<T>はTが持つcodeプロパティの型になる
type Code<T> = T extends { code: infer U } ? U : never;

```

## Promise の戻り値の型

```
//Tがプロミスを返す関数だった場合、その返却値の型がValue<T>の型となる
type Value<T> = T extends () => Promise<infer U> ? U : never;
//上記のValue<T>を返却するPromise関数の型がFetch<T>である
type Fetch<T> = () => Promise<Value<T>>;

//promise配列を渡すと順番に処理する関数
async function series<T>(...fetches: Fetch<T>[]) {
  let responses: Value<T>[] = [];
  for (let fetch of fetches) {
    responses.push(await fetch());
  }

  return responses;
}


series<() => Promise<{ data: string; }>>(
  () => Promise.resolve({ ddata: 'first' }), // コンパイルエラー
  () => Promise.resolve({ data: 'second' }),
  () => Promise.resolve({ data: 'third' }),
).then( responses => responses.forEach(response => {
    console.log(response.dataa); // コンパイルエラー
  })
);
```

## Mappged Types

```js
type Item = { a: string; b: number; c: boolean };

//xとyにnumber型をセットしたtypeを返却
type T1 = { [P in 'x' | 'y']: number }; // { x: number, y: number }
//xとyにそれぞれキー名をセットしたtypeを返却
type T2 = { [P in 'x' | 'y']: P }; // { x: "x", y: "y" }
//xとyにItemにある同名の型をセットしたtypeを返却
type T3 = { [P in 'a' | 'b']: Item[P] }; // { a: string, b: number }
//Itemのキーを全て列挙して全てDate型をセットしたtypeを返却
type T4 = { [P in keyof Item]: Date }; // { a: Date, b: Date, c: Date }
//Itemのキーを全て列挙して全てItemと同一の型をセットしたtypeを返却
type T5 = { [P in keyof Item]: Item[P] }; // { a: string, b: number, c: boolean }
//Itemのキーを全て列挙して全てItemと同一の型をセットしつつreadonlyにしたtypeを返却
type T6 = { readonly [P in keyof Item]: Item[P] }; // { readonly a: string, readonly b: number, readonly c: boolean }
//Itemのキーを全て列挙して全ての型をその型の配列にしたtypeを返却
type T7 = { [P in keyof Item]: Array<Item[P]> }; // { a: string[], b: number[], c: boolean[] }
```

## 既存の Object から型を取得

```js
interface User {
  name: string;
  age: number;
}
または
type User = {
  name: string;
  age: number;
};

type UserKeys = keyof User;// 'name'か'age'しか受け付けない型
type UserName = User['name']; //string型
type UserAge = User['age']; //number型
```

## 基本

```js
//連想配列のキーを指定する
type Hash = {
  [key: string]: string,
};
```

## ジェネリクス

```js

/**
 * 文字列だけを受け付ける
 */
function action<T extends string>(actionName: T) {
  return `「${actionName}」`;
}

/**
 * オブジェクト内の指定のキーの内容の配列を返却する
 * 渡されたTのObjectのキーが第二引数で指定されているかチェックしている
 */
function pluck<T, K extends keyof T>(o: T, names: K[]): T[K][] {
  return names.map((n) => o[n]);
}

//第二引数にa、b、c、nyao以外のストリングを指定するとtsエラーが出現する
const p2 = pluck({ a: 1, b: 2, c: 3, nyao: 'ニャオ' }, ['a', 'c', 'nyao']);
console.log('pluck', { ...p2 }); // pluck Object(3) [ 1, 3, "ニャオ" ]

//同じようにオブジェクトの中身を取り出すとき、指定のキーが存在するかtsがチェックする
function getProperty<T, K extends keyof T>(o: T, name: K): T[K] {
  return o[name]; // o[name] は T[K] の型の一つ
}

/**
 * オブジェクトにキーとバリューをセットする
 * そのときキーはオブジェクトに存在するものしか指定不可
 * さらに入力値の型は変更不可
 */
function setValue<T, K extends keyof T, U extends T[K]>(
  obj: T,
  key: K,
  value: U,
): T {
  obj[key] = value;
  return obj;
}
const o = { hoe: 5 };
console.log('setValue', setValue(o, 'hoe', 454));

/**
 * idプロパティを持つオブジェクトしか受け付けない
 * もしnumプロパティがあるならば、ddプロパティを持つ必要がある
 */
export function merge<
  T extends {
    id: number;
    num?: X;
  },
  X extends {
    dd: string;
  }
>(array: T[], newValue: T): T[] {
  const index = array.findIndex((item) => item.id === newValue.id);
  if (index === -1) {
    return [...array, newValue];
  } else {
    return [...array.slice(0, index), newValue, ...array.slice(index + 1)];
  }
}
```

## Partial

プロパティを全て省略可能にした型を返却する

```
type Actions = {
  jump1: string;
  jump2: string;
};
const actionsPartial: Partial<Actions> = {
  jump2: "jump3333",
};

```

## Promise

```
function myPromise<T>(param: T): Promise<T> {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(param);
    }, 1000);
  });
}

(async () => {
  const ret = await myPromise<{ promise: string }>({
    promise: 'PROMISE',
  });
  console.log(ret);
})();
```

## 関数

関数として呼び出せる型は以下のように表す

```
type Callable = {
  (): void;
};
```

```
//関数を定義して、さらにプロパティsssも追加している
interface GenericIdentityFn {
    <T>(arg: T): T;
    sss?: number;
}
function identity<T>(arg: T): T {
    return arg;
}

let myIdentity: GenericIdentityFn = identity;
myIdentity.sss = 45;


//上記interfaceはtypeに変えても動く
type GenericIdentityFn = {
    <T>(arg: T): T;
    sss?: number;
}
```
