# 便利な型

## プロパティのAとBのどちらかが必須。でも両方あるのはダメ

## RequireOnlyOne

```
type RequireOnlyOne<T, Keys extends keyof T = keyof T> =
    Pick<T, Exclude<keyof T, Keys>>
    & { [K in Keys]-?:
        Required<Pick<T, K>>
        & Partial<Record<Exclude<Keys, K>, undefined>>
    }[Keys]

```
使ってみる
```
type MyItem = {    
  sho: number;
  chiku?: number;
  bai?: number;
};
//chikuもしくはbaiのどちらかだけ必須（省略すれば全てのプロパティについてどれか一つだけ存在可となる）
type ShoAndChikuOrBai = RequireOnlyOne<MyItem, 'chiku'|'bai'>

//エラー（chikuとbai両方ある）
const chikubai: ShoAndChikuOrBai = {
    sho: 44,
    chiku: 24,
    bai: 54,
}

```

