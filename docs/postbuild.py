import subprocess
from pyquery import PyQuery


def changePath(url):
    if url == '':
        return
    # 読み込む
    handler = open(url, 'r', encoding='utf-8')
    html = handler.read()
    handler.close()

    html = html.replace('src="/img/', 'src="/blog/img/')

    pq = PyQuery(html, parser='html')

    pq('.navbar-nav li:last-child').remove()
    # print(pq('.navbar-nav li:last-child'))

    html = pq.outerHtml()

    # 書き込む
    handler = open(url, 'w', encoding='utf-8')
    handler.write(html)
    handler.close()


res = ""
try:
    cmd = 'find ../public -name *.html'
    res = subprocess.check_output(
        'find ../public -name "*.html"', shell=True, universal_newlines=True)
except:
    print("Error.")

ary = res.split('\n')
for url in ary:
    changePath(url)
