
.PHONY: front
front:
	cd front && npm ci && npm run build

.PHONY: docs
docs:
	rm -rf public
	cd docs && mkdocs build
	cd docs && mv site ../public
	cd docs && python postbuild.py

dserver:
	cd docs && npm run sass &
	cd docs && mkdocs serve

.PHONY: nuxt
nuxt:
	cd blog-toshick && npm ci && npm run generate