const path = require('path');
const webpack = require('webpack');
// const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const rootPath = path.resolve(__dirname, '../');

module.exports = {
  stories: ['../stories/**/*.stories.js'],
  addons: ['@storybook/addon-actions', '@storybook/addon-links', '@storybook/addon-backgrounds/register'],

  webpackFinal: async (config, { configType }) => {
    // `configType` has a value of 'DEVELOPMENT' or 'PRODUCTION'
    // You can change the configuration based on that.
    // 'PRODUCTION' is used when building the static version of storybook.

    config.resolve.alias['~'] = rootPath;
    config.resolve.alias['@'] = rootPath;

    // Make whatever fine-grained changes you need
    config.module.rules.push(
      {
        test: /\.sass$/,
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
          },
          {
            loader: 'sass-loader',
            options: {
              sassOptions: {
                indentedSyntax: true,
              },
            },
          },
          {
            loader: 'sass-resources-loader',
            options: {
              resources: ['assets/sass/app.sass'],
            },
          },
        ],
      },
      {
        test: /\.pug$/,
        use: 'pug-plain-loader',
      },
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'ts-loader',
            options: {
              appendTsSuffixTo: [/\.vue$/],
              transpileOnly: true, // used with ForkTsCheckerWebpackPlugin
            },
          },
        ],
      },
      // {
      //   test: /\.(png|jpe?g|gif)$/i,
      //   use: [
      //     {
      //       loader: 'url-loader',
      //       options: {
      //         publicPath: 'static',
      //       },
      //     },
      //   ],
      // },
    );

    const mockD = {
      '@/store': '../stories/mock/mock-store.ts',
      '@/common/util': '../common/util.ts',
      '@/mixin/mxin': '../stories/mock/mock-mixin.ts',
    };
    // @storeをモック
    config.plugins.push(
      new webpack.NormalModuleReplacementPlugin(/@\/(store|common|mixin)/, (resource) => {
        const { request } = resource;
        console.log('request', request);

        const absRootMockPath = path.resolve(__dirname, mockD[request]);
        const relativePath = path.relative(resource.context, absRootMockPath);
        // Updates the `resource.request` to reference our mocked module instead of the real one
        resource.request = relativePath;
      }),
    );

    // Return the altered config
    return config;
    // return { ...config, module: { ...config.module, rules: custom.module.rules }
  },
};
