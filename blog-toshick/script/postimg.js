const execSync = require('child_process').execSync;
const fs = require('fs');
// const text = fs.readFileSync('./imglist.txt', 'utf-8');

const jsonData = {};
setJsonAt('../assets/conversation', jsonData);
setJsonAt('../assets/conversation/hello', jsonData);

// jsonファイルを出力
fs.writeFileSync('../assets/conversation.json', JSON.stringify({ conversation: jsonData }));

/**
 * setJsonAt
 */
function setJsonAt(path, d) {
  const text = execSync(`ls -F ${path} | grep -v /`).toString();
  const files = text.split('\n').filter((i) => i);

  files.forEach((f) => {
    const url = `${path}/${f}`;
    const name = f.replace('.yml', '');
    const result = execSync(`yq '.' ${url}`).toString();
    d[name] = JSON.parse(result).data;
  });
}
