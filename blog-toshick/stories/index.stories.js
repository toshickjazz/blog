import Vue from 'vue';
import Buefy from 'buefy';
import { ValidationProvider, ValidationObserver } from 'vee-validate';
import { withBackgrounds } from '@storybook/addon-backgrounds';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
import { storiesOf, addParameters } from '@storybook/vue';
// import { library } from '@fortawesome/fontawesome-svg-core';
// import { fas } from '@fortawesome/free-solid-svg-icons';
// import { fab } from '@fortawesome/free-brands-svg-icons';
// import { far } from '@fortawesome/free-regular-svg-icons';
// import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import VueButtonPug from '../components/VueButtonPug.vue';
import Header from '../components/Header.vue';
import Footer from '../components/Footer.vue';
import UserIcon from '../components/UserIcon.vue';
import TagList from '../components/TagList.vue';
import TagEdit from '../components/TagEdit.vue';
import CommentEdit from '../components/CommentEdit.vue';
// top
import Chunum from '../pages/-assets/Chunum.vue';
import SideMenu from '../pages/-assets/SideMenu.vue';
import PostImgItem from '../pages/-assets/PostImgItem.vue';

import OInput from '../components/form/OInput.vue';
import 'buefy/dist/buefy.css';
import './fontawesome-free-5.13.0-web/css/all.css';
import '../assets/sass/app.sass';
import './sass/storybook.sass';

Vue.use(Buefy);
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('OInput', OInput);

// addParameters({
//   backgrounds: [{ name: 'twitter', value: '#EAC795', default: true }],
// });

const postitem = {
  id: 'iddddddddddddd',
  imgurl: '',
  imgurlTh: 'https://storage.googleapis.com/toshickcom-a7f98.appspot.com/upload_images/Camera_2020-05-31_15.35.38-1590906948412.jpeg',
  imgurlMid: 'https://storage.googleapis.com/toshickcom-a7f98.appspot.com/upload_images/Camera_2020-05-31_15.35.38-1590906948412.jpeg',
  username: 'ユーザ名',
  created: '2020.06.01',
  comment: 'コメントですよ',
  sortkey: 'sorrrrt',
  tags: ['タグ1', 'タグ2', 'タグ3'],
};

/**
 * デバッグ
 */
storiesOf('top', module)
  .add('Chunum', () => ({
    components: { Chunum },
    template: `<div class="preview-margin"><Chunum /></div>`,
  }))
  .add('SideMenu', () => ({
    components: { SideMenu },
    template: `<div class="preview-margin sidemenu"><SideMenu /></div>`,
  }))
  .add('PostImgItem', () => ({
    components: { PostImgItem },
    data: () => {
      return {
        item: postitem,
      };
    },
    template: `<div class="preview-margin sidemenu"><PostImgItem :item="item" /></div>`,
  }));

/**
 * パーツ
 */
storiesOf('パーツ', module)
  .add('Header', () => ({
    components: { Header },
    template: `<Header  />`,
  }))
  .add('Footer', () => ({
    components: { Footer },
    template: `<Footer />`,
  }))
  .add('UserIcon', () => ({
    components: { UserIcon },
    template: `<div class="preview-margin"><UserIcon :logined="true" /></div>`,
  }))
  .add('TagList', () => ({
    components: { TagList },
    template: `<TagList />`,
  }))
  .add('TagEdit', () => ({
    components: { TagEdit },
    template: `<div class="preview-margin"><TagEdit :postimg="{}" /></div>`,
  }))
  .add('CommentEdit', () => ({
    components: { CommentEdit },
    template: `<div class="preview-margin"><CommentEdit /></div>`,
  }));

// import VueButtonPug from '../components/VueButtonPug.vue';
// import MyButton from './MyButton';

// export default {
//   title: 'いんでっくす',
//   component: MyButton,
// };

// export const Text = () => ({
//   components: { MyButton },
//   template: '<my-button @click="action">Hello Button</my-button>',
//   methods: { action: action('clicked') },
// });

// export const Jsx = () => ({
//   components: { MyButton },
//   render(h) {
//     return <my-button onClick={this.action}>With JSX</my-button>;
//   },
//   methods: { action: linkTo('clicked') },
// });

// export const Emoji = () => ({
//   components: { MyButton },
//   template: '<my-button @click="action">😀 😎 👍 💯</my-button>',
//   methods: { action: action('clicked') },
// });

// export const VueButtonPugCompo = () => ({
//   components: { VueButtonPug },
//   template: '<VueButtonPug>rounded</VueButtonPug>',
//   methods: { action: action('clicked') },
// });
