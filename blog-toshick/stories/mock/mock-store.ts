import { Tag, PostImg } from '@/plugins/firebase.ts';

const taglist: Tag[] = [
  { id: '33', title: 'タグ001' },
  { id: '44', title: 'タグ002' },
  { id: '55', title: 'タグ003' },
  { id: '66', title: 'タグ004' },
  { id: '77', title: 'タグ005' },
];

const postimglist: PostImg[] = [];

/**
 * appStore
 */
export const appStore = {
  logined: true,
  isAdmin: false,
};

/**
 * imgpostStore
 */
export const imgpostStore = {
  taglist,
  postimglist,
  UpdatePost: () => {},
  RemoveTag: () => {},
  AddTag: () => {},
};
