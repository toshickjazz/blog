import dayjs from 'dayjs';
import { createClient } from 'contentful';
import Vue from 'vue';
import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { asort } from '@/common/util';
// import { getRequest, postRequest } from '@/common/request';
import { setUpPostImg, firebase, postImgRef, tagRef, Tag, PostImg, PostImgUpdate, TagUpdate, PostImgRequest } from '@/plugins/firebase.ts';
import { ActionRes } from '@/types/app';

@Module({ name: 'imgpost', stateFactory: true, namespaced: true })
export default class MyClass extends VuexModule {
  postimglist: PostImg[] = [];
  taglist: Tag[] = [];

  // ----------------------
  // postimg
  // ----------------------
  @Mutation
  RESET_POSTIMG() {
    this.postimglist = [];
  }

  @Mutation
  ADD_POSTIMG(postimg: PostImg) {
    const ary = [...this.postimglist];
    ary.unshift(postimg);
    this.postimglist = asort(ary, 'sortkey').reverse();
    // this.postimglist = ary;
  }

  @Mutation
  REMOVE_POSTIMG(url: string) {
    this.postimglist = this.postimglist.filter((i: PostImg) => i.imgurl !== url);
  }

  @Mutation
  UPDATE_POSTIMG(postimg: PostImg) {
    console.log('UPDATE_POSTIMG', postimg);

    this.postimglist = this.postimglist.map((i: PostImg) => {
      if (i.id === postimg.id) {
        i = postimg;
      }
      return i;
    });
  }

  // ----------------------
  // postimg
  // ----------------------
  @Mutation
  ADD_TAG(tag: Tag) {
    const ary = [...this.taglist];
    ary.unshift(tag);
    this.taglist = asort(ary, 'id');
  }

  @Mutation
  REMOVE_TAG(id: string) {
    this.taglist = this.taglist.filter((i: Tag) => i.id !== id);
  }

  @Mutation
  UPDATE_TAG(tag: Tag) {
    this.taglist = this.taglist.map((i: Tag) => {
      if (i.id === tag.id) {
        return tag;
      }
      return i;
    });
  }

  // ----------------------
  // imgpost
  // ----------------------

  @Action
  UpdatePost(params: PostImgUpdate): Promise<ActionRes> {
    if (!params.id) {
      console.log('idの指定がないぞ');
      return Promise.resolve({ error: true });
    }
    const { id } = params;
    const p: PostImgUpdate = { ...params };
    return postImgRef
      .where('imgurl', '==', id)
      .get()
      .then(function(querySnapshot: firebase.firestore.QuerySnapshot) {
        querySnapshot.forEach((doc: any) => {
          postImgRef.doc(doc.id).update(p);
        });
        return { ok: true };
      })
      .catch(function(error: any) {
        console.log('Error getting documents: ', error);
        return { error };
      });
  }

  @Action
  RemovePost(id: string): Promise<ActionRes> {
    return postImgRef
      .where('imgurl', '==', id)
      .get()
      .then(function(querySnapshot: firebase.firestore.QuerySnapshot) {
        querySnapshot.forEach((doc: any) => {
          postImgRef.doc(doc.id).delete();
        });
        return { ok: true };
      })
      .catch(function(error: any) {
        console.log('Error getting documents: ', error);
        return { error };
      });
  }

  // ----------------------
  // tag
  // ----------------------
  @Action
  UpdateTag(params: TagUpdate): Promise<ActionRes> {
    if (!params.id) {
      console.log('idの指定がないぞ');
      return Promise.resolve({ error: true });
    }
    const { id } = params;
    const p: TagUpdate = { ...params };
    return tagRef
      .where('id', '==', id)
      .get()
      .then(function(querySnapshot: firebase.firestore.QuerySnapshot) {
        querySnapshot.forEach((doc: any) => {
          tagRef.doc(doc.id).update(p);
        });
        return { ok: true };
      })
      .catch(function(error: any) {
        console.log('Error getting documents: ', error);
        return { error };
      });
  }

  @Action
  AddTag(params: Tag): Promise<ActionRes> {
    if (!params.id) {
      console.log('idの指定がないぞ');
      return Promise.resolve({ error: true });
    }

    const p: Tag = { ...params };

    return tagRef
      .add(p)
      .then((docRef: any) => {
        console.log('Document written with ID: ', docRef.id);
        return { ok: true };
      })
      .catch((error: any) => {
        console.error('Error adding document: ', error);
        return { error };
      });
  }

  @Action
  RemoveTag(id: string): Promise<ActionRes> {
    return tagRef
      .where('id', '==', id)
      .get()
      .then(function(querySnapshot: firebase.firestore.QuerySnapshot) {
        querySnapshot.forEach((doc: any) => {
          tagRef.doc(doc.id).delete();
        });
        return { ok: true };
      })
      .catch(function(error: any) {
        console.log('Error getting documents: ', error);
        return { error };
      });
  }

  @Action
  FilterWithTag(p: PostImgRequest) {
    this.RESET_POSTIMG();
    setUpPostImg(p);
  }

  // ----------------------
  // get
  // ----------------------
  // get logined(): boolean {
  //   return checkAuth();
  // }
}
