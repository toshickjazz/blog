import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { sleep } from '@/common/util';

export type Comments = string[];
export type CommentQ = {
  label: string;
  value: string;
};
export type Answer = { [anskey: string]: CommentItem };
export type CommentItem = {
  strs: Comments;
  q?: CommentQ[];
  answer?: any;
};

@Module({ name: 'conversation', stateFactory: true, namespaced: true })
export default class MyClass extends VuexModule {
  // itemsAry: CommentStoreItem[] = [];
  // ----------------------
  // Mutation
  // ----------------------
  // @Mutation
  // ADD_CONVERSATION({ name, item }: { name: string; item: ConversationClass }) {
  //   this.itemsAry.push({ name, cclass: item });
  // }
  // ----------------------
  // Action
  // ----------------------
  // @Action
  // createConversation(name: string): ConversationClass {
  //   const item = new ConversationClass(comments);
  //   this.ADD_CONVERSATION({ name, item });
  //   return item;
  // }

  @Action
  FetchEntries() {
    return 555;
  }

  @Action
  async GetScreenItemsJson() {
    const url = '/figmajson.json';
    const res = await fetch(url);
    return res.json();
  }
  // ----------------------
  // get
  // ----------------------
  // get storeItems(): CommentStoreItem[] {
  //   return this.itemsAry;
  // }
  // ----------------------
  // get
  // ----------------------
  // get talking(): boolean {
  //   // return true;
  //   if (this.items.chunum) {
  //     // return this.items.chunum.talking || true;
  //     return true;
  //   }
  //   return false;
  // }
  // ----------------------
  // get
  // ----------------------
  // get itemKeys(): string[] {
  //   return this.itemsAry.map((i: CommentStoreItem) => i.name);
  // }
}
