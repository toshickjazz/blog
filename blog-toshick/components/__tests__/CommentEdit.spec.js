import { renderToString } from '@vue/server-test-utils';
import '@/test/setup.js';
import Vue from 'vue';
import { mount } from '@vue/test-utils';

import CommentEdit from '@/components/CommentEdit.vue';

// wrapper.vm.$refs.emailField.currentValue;

describe('CommentEdit', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(CommentEdit);
    expect(wrapper).toBeTruthy();
    expect(wrapper.element).toMatchSnapshot();
  });

  // FIX: うまくいかん
  test('保存ボタン制御', (done) => {
    const wrapper = mount(CommentEdit);
    const btn = wrapper.find('.btn-save');
    expect(btn.attributes().disabled).toBe('disabled');
    expect(btn.attributes().ddd).toBeFalsy();

    const textarea = wrapper.find('textarea');
    expect(textarea.exists()).toBeTruthy();
    textarea.setValue('テキストです');

    Vue.nextTick(() => {
      // expect(btn.attributes().disabled).toBeFalsy();
      done();
    });
  });
  /**
   * キャンセルクリック
   */
  test('キャンセルクリック', () => {
    const wrapper = mount(CommentEdit);
    const btn = wrapper.find('.btn-cancel');
    btn.trigger('click');

    expect(wrapper.emitted('close')).toBeTruthy();
  });
  /**
   * renderToString
   */
  test('renderToString', async () => {
    const str = await renderToString(CommentEdit);
    expect(str).toContain('btn-save');
  });
});
