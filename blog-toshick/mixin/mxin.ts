import querystring from 'querystring';
import Vue from 'vue';

/**
 * outClickClose
 */
export const outClickClose = Vue.extend({
  mounted() {},

  beforeDestroy() {
    this.setWindowClick(false);
  },

  methods: {
    setWindowClick(flg: boolean) {
      window.removeEventListener('click', this.windowClick);
      if (flg) {
        window.addEventListener('click', this.windowClick);
      }
    },
    windowClick(e: MouseEvent) {
      if (!this.$el.contains(e.target as Element)) {
        this.onClickOutSide();
      }
    },

    onClickOutSide() {},
  },
});

/**
 * viewMixin
 */
export const viewMixin = Vue.extend({
  computed: {},
  mounted() {
    window.addEventListener('hashchange', this.hashchange);
  },

  beforeDestroy() {
    window.removeEventListener('hashchange', this.hashchange);
  },

  methods: {
    hashchange(e: HashChangeEvent) {
      this.onChangeHash(e.newURL.split('#')[1]);
    },
    onChangeHash(hash: string) {},

    getUrlParms(url?: string) {
      const u = url || location.hash;
      if (!u) return {};
      return querystring.parse(u.replace('#', ''));
    },

    setUrlParms(params: { [key: string]: string }) {
      const p: any = {};
      for (const [key2, val2] of Object.entries(params)) {
        if (val2) p[key2] = val2;
      }
      if (!p.total) p.total = '30';
      const items = [];
      for (const [key2, val2] of Object.entries(p)) {
        items.push(`${key2}=${val2}`);
      }
      location.hash = items.join('&');
    },
  },
});
