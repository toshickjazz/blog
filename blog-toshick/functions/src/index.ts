// import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
// import * as dayjs from 'dayjs';
// import { Storage } from '@google-cloud/storage';
import fileUpload from './upload';
// import addMessage from './add-message';

// The Firebase Admin SDK to access the Firebase Realtime Database.
admin.initializeApp();
const fireStore = admin.firestore();

/**
 * addMessage
 */
// exports.addMessage = addMessage();

/**
 * fileUpload
 */
exports.fileUpload = fileUpload(fireStore);
