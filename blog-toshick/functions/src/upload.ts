import * as path from 'path';
import * as os from 'os';
import * as fs from 'fs';
import axios from 'axios';
import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as Busboy from 'busboy';
import * as S from '@google-cloud/storage';
import * as sharp from 'sharp';
import * as sizeOf from 'image-size';

type Tag = {
  id: string;
  title: string;
};

type RecordPostImg = {
  timestamp: number;
  created: string;
  imgurl: string;
  username: string;
  comment: string;
  tags: Tag[];
};

type UploadData = {
  [key: string]: { filepath: string; mimeType: string; filename: string };
};

const BUCKET = 'gs://toshickcom-a7f98.appspot.com';
/**
 * fileupload
 */
export default (fireStore: admin.firestore.Firestore) => {
  const storage = new S.Storage();

  /**
   * createFileName (time stamp)
   */
  const createFileName = (originalFilename: string) => {
    const d = new Date();
    const year = `${d.getFullYear()}`.padStart(2, '0');
    const month = `${d.getMonth() + 1}`.padStart(2, '0');
    const date = `${d.getDate()}`.padStart(2, '0');
    const hour = `${d.getHours() + 9}`.padStart(2, '0');
    const min = `${d.getMinutes()}`.padStart(2, '0');
    const sec = `${d.getSeconds()}`.padStart(2, '0');
    const milsec = `${d.getMilliseconds()}`.padStart(3, '0');
    const extension = originalFilename.replace(/.+\./, '');
    return `${year}-${month}${date}-${hour}${min}-${sec}-${milsec}.${extension}`;
  };

  /**
   * addPostIMgRecord
   */
  const addPostIMgRecord = (params: RecordPostImg): Promise<any> => {
    const citiesRef = fireStore.collection('postimg').doc();
    return citiesRef
      .set(params)
      .then((res) => {
        console.log('レコード追加成功', res);
        return res;
      })
      .catch((err) => {
        console.warn('レコード追加に失敗', err);

        return { error: true, msg: err.message };
      });
  };

  /**
   * uploadToBucket
   */
  const uploadToBucket = async (bucket: S.Bucket, filepath: string, destination: string, mimeType: string): Promise<string> => {
    // const name = getBucketFileName(filepath);
    // const destination = `upload_images/${name}`;
    if (!exists(filepath)) {
      console.log('ファイルないぞ', filepath);
      return Promise.resolve('');
    }
    let retryCount = 0;

    return new Promise((resolve) => {
      doUpload();

      function doUpload() {
        retryCount += 1;

        // if (retryCount === 1) {
        //   console.log(`doUpload  ${retryCount}  回目`);
        // } else {
        //   console.log(`doUpload  ${retryCount}  回目`);
        // }

        bucket
          .upload(filepath, {
            destination,
            metadata: { contentType: mimeType },
            public: true,
          })
          .then(() => {
            console.log('アップロード成功: ', destination);
            resolve(destination);
          })
          .catch((err: any) => {
            console.error('アップロードエラー');
            console.log(err);
            if (retryCount === 3) {
              console.error(`doUpload 合計${retryCount}回トライした。あきらめた`);
              return;
            }
            doUpload();
          });
      }
    });
  };

  /**
   * removeFile
   */
  const removeFile = (filepath: string) => {
    if (!exists(filepath)) {
      console.warn('削除ファイルないぞ', filepath);
      return Promise.resolve('');
    }

    return new Promise((resolve) => {
      fs.unlink(filepath, (err: any) => {
        if (err) {
          console.warn('file removed エラー');
        } else {
          console.log('file 削除: ' + filepath);
        }
        resolve('');
      });
    });
  };

  /**
   * postSlack
   */
  const postSlack = (urls: string[]) => {
    return axios({
      method: 'POST',
      url: 'https://hooks.slack.com/services/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',
      data: {
        text: `postありました ${urls.join(' ')}`,
      },
    }).catch(() => {
      console.error(`postSlack失敗`);
    });
  };

  /**
   * postNelify
   */
  const postNelify = (groupid: string, url: string, thumbnail: string) => {
    return axios({
      method: 'POST',
      url: 'https://shorcut-trigger.netlify.app/.netlify/functions/post-line',
      data: {
        imageUrlOri: url,
        imageUrlPreview: thumbnail,
        groupid, // line chat group id
      },
    }).catch(() => {
      console.error(`postNelify失敗`);
    });
  };

  /**
   * addRecordFirebase
   */
  const addRecordFirebase = (uploads: UploadData, bucket: S.Bucket): Promise<string[]> => {
    console.log('addRecordFirebase', uploads);

    const imgurls: string[] = [];
    let retryCount = 0;

    return new Promise((resolve) => {
      addRecord();

      function addRecord() {
        retryCount += 1;

        if (retryCount === 1) {
          console.log(`addRecord  ${retryCount}  回目`);
        } else {
          console.log(`addRecord  ${retryCount}  回目`);
        }

        const ps = [];
        for (const d of Object.entries(uploads)) {
          // fireStoreに保存
          const { filepath } = d[1];
          const imgurl = `https://storage.googleapis.com/${bucket.name}/${filepath}`;
          const t = new Date();
          const r: RecordPostImg = {
            created: t.toString(),
            imgurl,
            username: 'from-function',
            timestamp: t.getTime(),
            comment: '',
            tags: [],
          };
          ps.push(addPostIMgRecord(r));
          imgurls.push(imgurl);
        }

        Promise.all(ps)
          .then(() => {
            console.log('成功しました！ : ' + JSON.stringify(uploads));
            resolve(imgurls);
          })
          .catch(() => {
            if (retryCount === 3) {
              console.error(`addRecord 合計${retryCount}回トライした。あきらめた`);
              return;
            }
            addRecord();
          });
      }
    });
  };

  const runtimeOpts: functions.RuntimeOptions = {
    timeoutSeconds: 500,
    // memory: '1GB',
    memory: '512MB',
    // memory: '256MB',
  };

  return functions.runWith(runtimeOpts).https.onRequest((req: any, res: any) => {
    // ↓ CORS
    if (req.method === 'OPTIONS') {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Headers', 'Content-Type');
      res.header('Access-Control-Allow-Methods', 'POST');
      res.status(200).end();
      return;
    }
    // ↑ CORS
    if (req.method !== 'POST') {
      res.status(405).send('Method Not Allowed');
      return;
    }
    console.log('------------------ 処理開始 ------------------');

    const busboy = Busboy({ headers: req.headers });
    // This object will accumulate all the uploaded files, keyed by their name.
    const uploads: UploadData = {};
    const allowMimeTypes = ['image/png', 'image/jpeg', 'image/gif'];
    const bucket = storage.bucket(BUCKET);

    const groupIdPromise = new Promise<string>((resolve) => {
      busboy.on('field', (fieldname: string, value: string) => {
        if (fieldname === 'groupid') {
          console.log('lineGroupIdを取得', value);
          resolve(value);
        }
      });
    });

    const uploadPromise = new Promise<string[]>((resolve) => {
      // This callback will be invoked for each file uploaded.
      busboy.on('file', (fieldname: string, file: NodeJS.ReadableStream, info: Busboy.FileInfo) => {
        const { mimeType } = info;
        if (!allowMimeTypes.includes(mimeType.toLocaleLowerCase())) {
          console.warn('disallow mimeType: ' + mimeType);
          return;
        }

        const filename = createFileName(info.filename);

        // Note that os.tmpdir() is an in-memory file system, so should
        // only be used for files small enough to fit in memory.
        const tmpdir = os.tmpdir();
        const filepath = path.join(tmpdir, filename);
        const writeStream = fs.createWriteStream(filepath);
        file.pipe(writeStream);
        console.log('filename', filename, 'filepath', filepath);

        writeStream.on('error', (err) => {
          console.log('writeStream error', err);
        });

        writeStream.on('finish', async () => {
          console.log('writeStream finish');
          const resizedUrlBig = await resizeImg(filepath, 1280);
          const name = getBucketFileName(filepath);
          const destination = `upload_images/${name}`;
          const uploadedUrl = await uploadToBucket(bucket, resizedUrlBig, destination, mimeType);

          uploads[fieldname] = { filepath: uploadedUrl, mimeType, filename };

          // リサイズ
          const resizedUrlMid = await resizeImg(resizedUrlBig || filepath, 480);
          if (resizedUrlMid) {
            console.log('サムネイルMid アップロード開始', resizedUrlMid);
            await uploadToBucket(bucket, resizedUrlMid, `upload_images_mid/${name}`, mimeType);
          }

          // リサイズ
          // const resizedUrlTh = await resizeImg(resizedUrlMid || filepath, 240);
          // if (resizedUrlTh) {
          //   console.log('サムネイルTh アップロード開始', resizedUrlTh);
          //   await uploadToBucket(bucket, resizedUrlTh, `upload_images_th/${name}`, mimeType);
          // }

          // tmpファイルを削除
          await removeFile(filepath);
          // if (resizedUrlTh) await removeFile(resizedUrlTh);
          if (resizedUrlMid) await removeFile(resizedUrlMid);

          const urls = await addRecordFirebase(uploads, bucket);
          resolve(urls);
        });
      });
    });

    Promise.all([groupIdPromise, uploadPromise]).then(async (values) => {
      console.log('Promise all');
      const groupid = values[0];
      const urls = values[1];
      if (urls) {
        // slack通知
        await postSlack(urls);
        const url = urls[0];
        const th = urls[0].replace('upload_images', 'upload_images_mid');
        await postNelify(groupid, url, th);
      }
      console.log('------------------ complete ------------------');
    });

    // The raw bytes of the upload will be in req.rawBody. Send it to
    // busboy, and get a callback when it's finished.
    busboy.end(req.rawBody);

    // this does not work
    // req.pipe(busboy);

    // すぐに返却
    res.status(200).send('ok');
  });
};

/**
 * バケット格納時のファイル名を返却
 */
function getBucketFileName(filepath: string) {
  const originalname = path.basename(filepath, path.extname(filepath));
  const extname = path.extname(filepath);
  return `${originalname}-${new Date().getTime()}${extname}`.replace(/ /g, '_');
}

/**
 * リサイズ
 */
async function resizeImg(filepath: string, width: number = 640): Promise<string> {
  if (!exists(filepath)) {
    return Promise.resolve('');
  }

  const dirname = path.dirname(filepath);
  const originalname = path.basename(filepath, path.extname(filepath));
  const extname = path.extname(filepath);
  const turl = `${dirname}/${originalname}-${width}${extname}`;

  const dimensions = await sizeOf.imageSize(filepath);
  const imgWidth = dimensions.width || 0;
  if (imgWidth === 0 || imgWidth <= width) {
    console.log(width, 'より小さいのでリサイズなし', filepath);
    return Promise.resolve(filepath);
  }

  return new Promise((resolve) => {
    sharp(filepath)
      .resize({ width })
      .toFile(turl, (err: any, info: any) => {
        if (err) {
          console.log('リサイズ失敗', err);
          resolve('');
        }
        resolve(turl);
      });
  });
}

/**
 * exists
 */
function exists(filepath: string): boolean {
  try {
    fs.statSync(filepath);
    return true;
  } catch ({ code }) {
    if (code === 'ENOENT') {
      console.warn('ファイル・ディレクトリは存在しません。', filepath);
    }
    return false;
  }
}
