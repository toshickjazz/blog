/* eslint import/no-mutable-exports: 0 */

import { Store } from 'vuex';
import { getModule } from 'vuex-module-decorators';
import AppClass from '~/store/app';
import ConversationClass from '~/store/conversation';
import ImgpostClass from '~/store/imgpost';
// import ProjectClass from '~/store/project';
// import RontenClass from '~/store/ronten';

let appStore: AppClass;
let conversationStore: ConversationClass;
let imgpostStore: ImgpostClass;

/**
 * initialiseStores
 */
function initialiseStores(store: Store<any>): void {
  appStore = getModule(AppClass, store);
  conversationStore = getModule(ConversationClass, store);
  imgpostStore = getModule(ImgpostClass, store);
}

export { initialiseStores, appStore, conversationStore, imgpostStore };
