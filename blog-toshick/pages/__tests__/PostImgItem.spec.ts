import '@/test/setup';
import { mount } from '@vue/test-utils';
import PostImgItem from '@/pages/-assets/PostImgItem.vue';
import { PostImgView } from '@/types/app';
import { appStore } from '@/store';

jest.mock('@/store', () => {
  return {
    appStore: {
      isAdmin: false,
    },
    imgpostStore: {},
  };
});

const postitem: PostImgView = {
  id: 'string',
  imgurl: 'string',
  imgurlTh: 'string',
  imgurlMid: 'string',
  username: 'かまれお',
  created: '2020.0101',
  comment: 'コメントです',
  sortkey: 999999,
  tagsDisp: [],
};

/**
 * getWrapper
 */
function getWrapper() {
  const wrapper = mount(PostImgItem, {
    propsData: {
      item: postitem,
    },
    directives: {
      dhover: jest.fn(),
    },
  });
  return wrapper;
}

describe('PostImgItem', () => {
  beforeEach(() => {
    jest.unmock('@/store');
    jest.resetAllMocks();
    jest.useFakeTimers();
  });
  afterEach(() => {
    // jest.resetAllMocks();
  });
  /**
   * test
   */
  test('is a Vue instance', () => {
    const wrapper = getWrapper();
    expect(wrapper).toBeTruthy();
  });
  /**
   * test
   */
  test('レンダーテスト', () => {
    jest.unmock('@/store');
    // jest.mock('@/store', () => {
    //   return {
    //     appStore: {
    //       isAdmin: true,
    //     },
    //     imgpostStore: {},
    //   };
    // });

    const wrapper = getWrapper();
    expect(wrapper.find('.btn-trash').exists()).toBeTruthy();
  });
});
