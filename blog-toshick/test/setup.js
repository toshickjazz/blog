import Vue from 'vue';
import Buefy from 'buefy';
import { ValidationProvider, ValidationObserver } from 'vee-validate';
import OInput from '@/components/form/OInput.vue';

Vue.use(Buefy);
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('OInput', OInput);

// jest.mock('@/store', () => {
//   return {
//     appStore: {},
//     imgpostStore: {},
//   };
// });
