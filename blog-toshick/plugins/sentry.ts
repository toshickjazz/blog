import Vue from 'vue';
import * as Sentry from '@sentry/browser';
import { Vue as VueIntegration } from '@sentry/integrations';

Sentry.init({
  dsn: 'https://04a642ec20944871bcfe398ac03907b9@o399316.ingest.sentry.io/5256225',
  integrations: [new VueIntegration({ Vue, attachProps: true })],
});
