import Vue from 'vue';
/**
 * カスタムディレクティブ
 */
const DELAY = 500;
Vue.directive('dhover', {
  bind(el: HTMLElement, binding) {
    if (typeof binding.value !== 'function') {
      return;
    }
    const func = binding.value;
    const ms = binding.arg ? +binding.arg : DELAY;
    setEvent(el, func, ms);
  },
  componentUpdated(el, binding) {
    // イベント削除
    el.dispatchEvent(new CustomEvent('removeAllHandlers'));

    const func = binding.value;
    const ms = binding.arg ? +binding.arg : DELAY;
    setEvent(el, func, ms);
  },
  unbind(el: HTMLElement) {
    // イベント削除
    el.dispatchEvent(new CustomEvent('removeAllHandlers'));
  },
});

/**
 * リスナーセットおよび削除イベントセット
 */
function setEvent(el: HTMLElement, func: any, ms: number) {
  let timerID: NodeJS.Timer | null = null;
  const menter = () => {
    if (timerID) clearTimeout(timerID);
    timerID = setTimeout(() => {
      func();
      timerID = null;
    }, ms);
  };
  const mleave = () => {
    if (timerID) clearTimeout(timerID);
    timerID = null;
  };

  el.addEventListener('mouseenter', menter);
  el.addEventListener('mouseleave', mleave);

  const removeAllHandlers = () => {
    el.removeEventListener('mouseenter', menter);
    el.removeEventListener('mouseleave', mleave);
    el.removeEventListener('removeAllHandlers', removeAllHandlers);
  };

  el.addEventListener('removeAllHandlers', removeAllHandlers);
}
