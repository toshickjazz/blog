import Vue from 'vue';
import { ValidationProvider, ValidationObserver } from 'vee-validate';
// import Vue2TouchEvents from 'vue2-touch-events';
import VueYoutube from 'vue-youtube';
import axios from 'axios';
import sanitizeHTML from 'sanitize-html';
import { appStore } from '@/store';
import OInput from '@/components/form/OInput.vue';
import OUpload from '@/components/form/OUpload.vue';
import Logo from '@/components/Logo.vue';
import Header from '@/components/Header.vue';
import Footer from '@/components/Footer.vue';
import '@/components/form/validation';

/**
 * use
 */
Vue.use(VueYoutube);
// Vue.use(Vue2TouchEvents);

// ValidationProvider;
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('OInput', OInput);
Vue.component('OUpload', OUpload);
Vue.component('Logo', Logo);
Vue.component('Header', Header);
Vue.component('Footer', Footer);

// sanitizehtml;
Vue.prototype.$sanitize = sanitizeHTML;

// axios settings
axios.defaults.headers.common['X-Requested-By'] = 'ronten-requested-by';
Vue.prototype.$http = axios;

/**
 * 拡大縮小禁止
 */
document.addEventListener('touchmove', mobileNoScroll, { passive: false });

function mobileNoScroll(e: any) {
  if (e.touches.length >= 2) {
    // デフォルトの動作をさせない
    e.preventDefault();
  }
}

/**
 * filter
 */
const postFilter = (value: string) => {
  if (!value) return value;

  let ret = value.replace(/!\[.+?\]\(\/\/.+?\)/g, (match: string) => {
    const tmp = match.match(/.+?(\/\/.+)\)/);
    if (tmp && tmp.length === 2) {
      return `<img src='${tmp[1]}?w=200' />`;
    }

    return match;
  });

  ret = ret.replace(/\n/g, '<br>');

  return ret;
};
Vue.filter('postFilter', postFilter);

Vue.prototype.$myfilters = {
  postFilter,
};

/**
 * default
 */
export default () => {
  if (document.URL.includes('isadmin=1')) {
    appStore.SET_ADMIN(true);
  }
};
