import { appStore } from '@/store';

export default () => {
  if (!appStore.logined) {
    appStore.Login({ email: process.env.USER_EMAIL || '', password: process.env.USER_PASS || '' });
  }
};
