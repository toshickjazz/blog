import Vue from 'vue';

Vue.mixin({
  computed: {
    isTouch() {
      return this.$window.width <= 1023;
    },
    isMobile() {
      return this.$window.width <= 768;
    },
    isTablet() {
      return this.$window.width < 1023 && this.$window.width >= 769;
    },
    isDesktop() {
      return this.$window.width < 1215 && this.$window.width >= 1024;
    },
  },
  methods: {},
});
