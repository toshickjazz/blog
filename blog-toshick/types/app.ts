/* eslint camelcase: "off" */
import { PostImg, Tag } from '@/plugins/firebase.ts';

export type FileItem = {
  file: File | null;
  filename: string;
  src?: string;
  txt?: string;
};

export type ValidationState = {
  passed: boolean;
  failed: boolean;
  errors: any[];
};

export type ActionRes = {
  error?: boolean;
  ok?: boolean;
};

export type PostImgView = PostImg & {
  tagsDisp: Tag[];
};
