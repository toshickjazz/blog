import Vue from 'vue';
// declare module '*.vue' {
//   export default Vue;
// }

declare module 'vue/types/vue' {
  interface Vue {
    // $auth: firebase.auth.Auth;
    // $firestore: firebase.firestore.Firestore;
    $myfilters: any;
    $sanitize: any;
    isTouch: boolean;
    isMobile: boolean;
    isTablet: boolean;
    isDesktop: boolean;
    setWindowClick: (flg: boolean) => void;
    getUrlParms: (url?: string) => any;
    setUrlParms: (params: { [key: string]: string }) => void;
  }
}

interface MyWindow extends Window {
  onYouTubeIframeAPIReady?(): void;
  YT: any;
  dataLayer: any[];
}
declare let window: MyWindow;
